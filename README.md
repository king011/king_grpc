# king_grpc

這是一個 grpc 服務器 框架

以模塊的 形式 組合了多種 靈活的 微服務

## 授權

1. 當 接收到 一個 grpc 請求時 服務器會先 解析出 請求的 url 

   * /king011_frame_grpc.session.Service/Login 解析爲 /session.service/login
   * /king011_frame_grpc.session.Service/Logout 解析爲 /session.service/logout

1. 將 url 和 用戶權限 中定義的路由比較 如果找到匹配的路由 則提供服務 否則返回 PermissionDenied 錯誤

## url 和 路由定義

路由定義在 數據庫中 其 內存結構 如下

```
// Router 定義了 grpc 接口 需要的 權限
type Router struct {
	// 唯一 id
	ID int64 `xorm:"pk autoincr 'id'"`

	// 父id 爲 ui 程式 提供了 一個 參考用的 分組 不影響服務器 路由 判斷
	PID int64 `xorm:"index notnull 'pid' default 0"`

	// 爲 ui 程式 提供了 一個 參考用的 顯示 名稱
	Name string `xorm:"notnull default ''"`

	// 授權值
	Val int32 `xorm:"unique notnull default 0"`

	// 此權限能夠訪問的 路由
	Rule string `xorm:"TEXT notnull"`
}
```

### Router

Router 是 支持多種 匹配模式

1. =:XXX XXX 是 使用 : 分隔的 字符串 url 和其中 任何一個字符串 相等 則 匹配
1. s:XXX XXX 是 使用 : 分隔的 字符串 url 是以 其中任何一個 字符串開始 則 匹配
1. e:XXX XXX 是 使用 : 分隔的 字符串 url 是 以 其中 任何一個 字符串結尾 則 匹配
1. %:XXX XXX 是一個正則表達式 url 符合 此正則 定義 則 匹配

> url 和 router 都 不區分 大小寫

# 子模塊

每個 模塊都是 獨立運作 並且互不相干的 定義在 module 檔案夾下

每個子 模塊都需要實現 Module 接口 並且在 init 中 註冊自己

```
// Module 定義了子模塊 接口
type Module interface {
	// ID 返回 唯一的子模塊 名稱
	ID() string

	// OnStart 初始化 回調 應該在此 執行 模塊 初始化
	OnStart(basePath string, data json.RawMessage)

	// RegisterGRPC 爲子模塊 註冊 grpc 服務
	RegisterGRPC(srv *grpc.Server)

	// OnReload 模塊重載配置 回調
	OnReload(basePath string, data json.RawMessage, tag string) (e error)

	// 通知 子模塊 清空 數據庫 緩存
	//
	// tag 是一個 自定義標記 通常是 表名 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearDBCache(tag string) error

	// 通知 子模塊 清空 緩存
	//
	// tag 是一個 自定義標記 通常是 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearCache(tag string) error

	// OnStop 停止 回調 應該在此 執行 資源釋放 和 停止模塊工作
	OnStop()

	// Client 返回 客戶端 接口
	//
	// 如果返回 nil 則 不支持 客戶端
	Client() OnClient
}
```

module 檔案夾下 有多個 已經實現了的 模塊 是很好的 參考


定義好模塊後海需要 在 main.go 中 import 模塊 因爲go不註冊動態加載


# 分隔字符

一些數據以字符串存儲多個值 使用特殊符號 ` 作爲 分隔符號

配置檔案的 Module 定義了 服務要啓動的 模塊 列表

# 授權值

所有 授權值 中 0 是 特殊的

所有 連接都 默認 擁有此值 建議將對公共開發的 路由 設置到此值

```
"s:/session.Service/Certificate:/shared.Service/"
```


* \[0,100\] 的授權值 被系統模塊使用 功能模塊 應該 使用 100 之外的 權限值
* < 0 的 值 通常 用作 測試

# 已使用 權限值

* **0** 所有公共服務
    * session 登入
	* public 提供了 常用的 公共服務
	* server_list 提供了 服務器 編碼查詢

* **2** 所有登入者的 共享服務
    * session 保活 返回session存儲信息
	* shared 提供了 常用的 用戶共享 服務

* **3** 提供了 用戶 管理功能
    * user 用戶 創建 管理

* **4** 提供了 系統 管理功能
    * system 系統 管理員


* **200** simple_file 模塊佔用

* **220** server_list 模塊佔用

```
delete from router;
insert router (id,name,val,rule) values 
    (1,"public",0,"s:/module.session.service/certificate:/module.public.service/:/module.server_list.Service/get"),
    (2,"session",2,"s:/module.session.service/:/module.shared.service/"),
    (3,"user",3,"s:/module.user.service/"),
    (4,"system",4,"s:/module.system.service/"),
	
    (200,"simple_file",200,"s:/module.simple_file.Service/"),

    (220,"server_list",220,"s:/module.server_list.Service/")
	;
```

# 模塊

1. 核心模塊
    * [session](module/session/README.md)
    * [user](module/user/README.md)
    * [system](module/system/README.md)

1. 公共模塊
    * [public](module/public/README.md)
    * [shared](module/shared/README.md)

1. 功能模塊
    * [simple_file](module/simple_file/README.md)
    * [server_list](module/server_list/README.md)