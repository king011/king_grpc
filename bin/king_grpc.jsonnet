// 定義一些 常量
local Millisecond = 1;
local Second = Millisecond * 1000;
local Minute = Second * 60;
local Hour = Minute * 60;
local Day = Hour * 24;
{
    // GRPC 服務器 配置
    GRPC:{
        // 服務器 工作地址
        Addr:":17000",

        // x509 證書路徑
        // 如果爲空 則使用 h2c
        CertFile:"",
        KeyFile:"",
        // cookie 過期 時間
        Cookie:Day,
    },
    // 要啓用的 子模塊 
    Module:[
        // 提供 公共服務
        "public",
        // 提供 session 功能
        "session",
        // 爲 登入用戶 提供 共享服務
        "shared",
        // 提供了用戶管理
        "user",
        // 提供了系統管理
        "system",
        // 爲 用戶 綁定一個 上傳 檔案 目前用於 備份 數據庫
        "simple_file",
        // 爲 手機提供 編碼和服務器信息 查詢 
        "server_list",
    ],
    // 子模塊 配置
    ModuleData:{
        "system":"this is a test",
    },
    // 檔案 root 目錄
    // 如果 爲空 則 使用 fileroot 作爲根目錄
    // 如果 爲相對路徑 則使用相對可執行檔案所在檔案夾 的相對路徑
	Fileroot:"",
    // DB 數據庫 配置
    DB:{
        // 數據庫 驅動
        Driver:"mysql",
        // 數據庫 連接字符串
        Source:"KingGRPC:12345678@/KingGRPC?charset=utf8",
        // 是否 顯示 SQL 指令
        //ShowSQL:true,
        // Ping 數據庫 時間間隔 如果 < 10 分鐘 則 不進行 ping
        Ping:Hour,
        // 數據庫 緩存 大小 如果 < 100 則 不使用 緩存
        Cache:1000,
    },
    // 日誌 配置
    Logger:{
        // 日誌 http 如果爲空 則不啓動 http
        //HTTP:"localhost:20800",
        // 日誌 檔案名 如果爲空 則輸出到控制檯
        //Filename:"logs/king_grpc.log",
        // 單個日誌檔案 大小上限 MB
        //MaxSize:    100, 
        // 保存 多少個 日誌 檔案
        //MaxBackups: 3,
        // 保存 多少天內的 日誌
        //MaxAge:     28,
        // 要 保存的 日誌 等級 debug info warn error dpanic panic fatal
        Level :"debug",
        // 是否要 輸出 代碼位置
        Caller:true,
    },
}