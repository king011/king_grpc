#!/bin/bash
#Program:
#       golang build scripts by generate tools
#
#       https://pub.dartlang.org/packages/generate
#Email:
#       zuiwuchang@gmail.com
function check(){
	if [ "$1" != 0 ] ;then
	exit $1
	fi
}
function ShowHelp(){
	echo "help                 : show help"
	echo "l/linux   [t/tar]    : build for linux"
	echo "d/darwin  [t/tar]    : build for darwin"
	echo "w/windows [t/tar]    : build for windows"
	echo "g/grpc               : build for proto to grpc"
	echo "d/dart output        : build for proto to dart grpc"
}

function goGRPC(){
  dir=$(cd $(dirname $BASH_SOURCE)/../ && pwd)
  if [[ "$OSTYPE" == "msys" ]]; then
    output=${dir:1:1}:${dir:2}/
  else
    output=$dir/
  fi
  ./grpc.sh go pb/ $output
}
case $1 in
	g|grpc)
    goGRPC
	;;

	d|dart)
		if [[ $2 == "" ]]; then
			echo please input output
			exit 1
		else
			./grpc.sh dart pb/ $2
		fi
	;;

	l|linux)
		export GOOS=linux

		echo go build -ldflags "-s -w" -o bin/king_grpc
		go build -ldflags "-s -w" -o bin/king_grpc
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=linux.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=linux.386.tar.gz
			fi
			cd bin && tar -zcvf $dst king_grpc
		fi
	;;

	d|darwin)
		export GOOS=darwin

		echo go build -ldflags "-s -w" -o bin/king_grpc
		go build -ldflags "-s -w" -o bin/king_grpc
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=darwin.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=darwin.386.tar.gz
			fi
			cd bin && tar -zcvf $dst king_grpc
		fi
	;;

	w|windows)
		export GOOS=windows

		echo go build -ldflags "-s -w" -o bin/king_grpc.exe
		go build -ldflags "-s -w" -o bin/king_grpc.exe
		check $?

		if [[ $2 == tar || $2 == t ]]; then
			dst=windows.amd64.tar.gz
			if [[ $GOARCH == 386 ]];then
				dst=windows.386.tar.gz
			fi
			cd bin && tar -zcvf $dst king_grpc.exe
		fi
	;;

	*)
		ShowHelp
	;;
esac
