package cmd

import (
	"king_grpc/cmd/client"

	"github.com/spf13/cobra"
)

func init() {
	var target string
	var protocol string
	cmd := &cobra.Command{
		Use:   "client",
		Short: "run client",
		Run: func(cmd *cobra.Command, args []string) {
			client.Run(target, protocol)
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&target,
		"target", "t",
		"localhost:17000",
		"connect target",
	)
	flags.StringVarP(&protocol,
		"protocol", "p",
		"h2c",
		"net protocol [h2c h2 h2-skip]",
	)
	rootCmd.AddCommand(cmd)
}
