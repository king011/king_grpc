package client

import (
	"context"
	"crypto/tls"
	"king_grpc/cookie"
	"king_grpc/module"
	"log"

	"sort"

	king_io "gitlab.com/king011/king-go/io"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

// Run 運行 客戶端
func Run(target, protocol string) {
	var opts []grpc.DialOption
	switch protocol {
	case "h2c":
		opts = append(opts, grpc.WithInsecure())
	case "h2":
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: false,
		})))
	case "h2-skip":
		opts = append(opts, grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{
			InsecureSkipVerify: true,
		})))
	default:
		log.Fatalln("not support protocol :", protocol)
	}
	conn, e := grpc.Dial(target, opts...)
	if e != nil {
		log.Fatalln(e)
	}
	client := _Client{
		conn: conn,
		io:   king_io.NewInputReader(),
	}
	client.Run()

}

// _Client 客戶端
type _Client struct {
	conn *grpc.ClientConn
	io   *king_io.InputReader

	cookie string
}

func (c *_Client) help(client []string) {
	c.io.Println("module :")
	for i := 0; i < len(client); i++ {
		c.io.Println("   ", client[i])
	}
	c.io.Println("\n    h for help")
	c.io.Println("    q for quit")
}
func (c *_Client) Run() {
	management := module.Single()
	client := management.Client()

	sort.Sort(sortString(client))
	c.help(client)

	var id string
	for {
		id = c.io.ReadString("input module")
		if id == "q" {
			break
		} else if id == "h" {
			c.help(client)
			continue
		}

		if management.OnClient(c, id) {
			break
		}
	}
	c.conn.Close()
}
func (c *_Client) IO() *king_io.InputReader {
	return c.io
}
func (c *_Client) GetConn() *grpc.ClientConn {
	return c.conn
}

// SetCookie set cookie
func (c *_Client) SetCookie(cookie string) {
	c.cookie = cookie
}

// GetCookie get cookie
func (c *_Client) GetCookie() string {
	return c.cookie
}
func (c *_Client) Background() (ctx context.Context) {
	ctx = context.Background()
	if c.cookie == "" {
		return
	}
	// 創建 帶 元數據 的 Context
	md := metadata.Pairs(
		cookie.CookieName, c.cookie,
	)
	ctx = metadata.NewOutgoingContext(ctx, md)
	return
}
