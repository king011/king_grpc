package client

type sortString []string

// Len is the number of elements in the collection.
func (arrs sortString) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs sortString) Less(i, j int) bool {
	return arrs[i] < arrs[j]
}

// Swap swaps the elements with indexes i and j.
func (arrs sortString) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}
