package cmd

import (
	"github.com/spf13/cobra"
	"king_grpc/cmd/daemon"
	"king_grpc/configure"
	"king_grpc/cookie"
	"king_grpc/logger"
	"king_grpc/utils"
	"log"
	"strings"
)

func init() {
	basePath := utils.BasePath()
	var filename string
	cmd := &cobra.Command{
		Use:   "daemon",
		Short: "run as a daemon",
		Run: func(cmd *cobra.Command, args []string) {
			filename = strings.TrimSpace(filename)
			if filename == "" {
				cmd.Help()
				abort()
			}
			// 加載 配置
			conf, e := configure.Init(basePath, filename)
			if e != nil {
				log.Fatalln(e)
			}
			// 初始化 日誌
			e = logger.Init(basePath, &conf.Logger)
			if e != nil {
				log.Fatalln(e)
			}
			// 初始化 cookie
			cookie.Init(basePath+"/"+cookie.FileName, conf.GRPC.Cookie)

			// 運行 服務
			daemon.Run()
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"configure", "c",
		basePath+"/"+configure.FileName,
		"configure file",
	)
	rootCmd.AddCommand(cmd)
}
