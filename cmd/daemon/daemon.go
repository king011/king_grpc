package daemon

import (
	"king_grpc/db/manipulator"
)

// Run 運行 服務
func Run() {
	// 初始化 數據庫
	manipulator.Init()

	// 運行 grpc 服務
	runGRPC()
}
