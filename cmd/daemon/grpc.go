package daemon

import (
	"context"
	"king_grpc/configure"
	"king_grpc/cookie"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"king_grpc/module"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

const (
	grpcPackage = "/king011_frame_grpc."
)

func checkPermissionDenied(ctx context.Context, method string) (e error) {
	// 解析 請求 url
	if !strings.HasPrefix(method, grpcPackage) {
		e = status.Error(codes.Unimplemented, method)
		return
	}
	method = "/" + strings.ToLower(method[len(grpcPackage):])

	// 獲取 當前 權限
	md, ok := metadata.FromIncomingContext(ctx)
	var val []int32
	if ok {
		session, ef := cookie.FromMD(md)
		if ef != nil {
			e = status.Errorf(codes.PermissionDenied, "%s %s", method, ef.Error())
			return
		} else if session != nil {
			val = session.Power
		}
	}

	// 驗證 權限 和 url 是否匹配
	if !manipulator.RouterMatch(method, val...) {
		e = status.Error(codes.PermissionDenied, method)
		return
	}
	return
}
func runGRPC() {
	cnf := configure.Single().GRPC

	// 創建 監聽 Listener
	l, e := net.Listen("tcp", cnf.Addr)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "listen"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// 創建 rpc 服務器
	var srv *grpc.Server
	opt := []grpc.ServerOption{
		grpc.UnaryInterceptor(
			func(ctx context.Context,
				req interface{},
				info *grpc.UnaryServerInfo,
				handler grpc.UnaryHandler) (interface{}, error) {
				if e := checkPermissionDenied(ctx, info.FullMethod); e != nil {
					return nil, e
				}
				return handler(ctx, req)
			},
		),
		grpc.StreamInterceptor(
			func(srv interface{},
				ss grpc.ServerStream,
				info *grpc.StreamServerInfo,
				handler grpc.StreamHandler) error {
				if e := checkPermissionDenied(ss.Context(), info.FullMethod); e != nil {
					return e
				}
				return handler(srv, ss)
			},
		),
	}
	if cnf.H2() {
		creds, e := credentials.NewServerTLSFromFile(cnf.CertFile, cnf.KeyFile)
		if e != nil {
			if ce := logger.Logger.Check(zap.FatalLevel, "NewServerTLSFromFile"); ce != nil {
				ce.Write(
					logger.GroupGRPC,
					zap.Error(e),
				)
			}
			os.Exit(1)
		}
		opt = append(opt, grpc.Creds(creds))
		srv = grpc.NewServer(
			opt...,
		)
		if logger.Logger.OutFile() {
			log.Println("h2 work at", cnf.Addr)
		}
		if ce := logger.Logger.Check(zap.InfoLevel, "h2 work"); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.String("addr", cnf.Addr),
			)
		}
	} else {
		srv = grpc.NewServer(
			opt...,
		)
		if logger.Logger.OutFile() {
			log.Println("h2c work at", cnf.Addr)
		}
		if ce := logger.Logger.Check(zap.InfoLevel, "h2c work"); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.String("addr", cnf.Addr),
			)
		}
	}
	// 啓用子模塊
	management := module.Single()
	management.Enable(configure.Single().Module)

	// 通知子模塊初始化
	management.OnStart()

	// 註冊 服務
	management.RegisterGRPC(srv)

	// 註冊 反射 到 服務 路由
	reflection.Register(srv)
	go func() {
		ch := make(chan os.Signal, 2)
		signal.Notify(ch,
			os.Interrupt,
			os.Kill,
			syscall.SIGTERM)
		for {
			sig := <-ch
			switch sig {
			case os.Interrupt:
				srv.Stop()
				return
			case syscall.SIGTERM:
				srv.Stop()
				return
			}
		}
	}()
	// 讓 rpc 在 Listener 上 工作
	if e := srv.Serve(l); e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "grpc Serve"); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		// 通知子模塊退出
		management.OnStop()
		os.Exit(1)
		return
	}
	// 通知子模塊退出
	management.OnStop()
}
