package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"king_grpc/cookie"
	"king_grpc/utils"
	"log"
	"strings"
)

func init() {
	basePath := utils.BasePath()
	var filename string
	cmd := &cobra.Command{
		Use:   "generate",
		Short: "generate secure cookie key",
		Run: func(cmd *cobra.Command, args []string) {
			filename = strings.TrimSpace(filename)
			if filename == "" {
				cmd.Help()
				abort()
			}
			hashKey, blockKey := cookie.Generate()
			e := cookie.Save(filename, hashKey, blockKey)
			if e != nil {
				log.Fatalln(e)
			}
			fmt.Println("generate secure key to : ", filename)
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&filename,
		"output", "o",
		basePath+"/"+cookie.FileName,
		"output file",
	)
	rootCmd.AddCommand(cmd)
}
