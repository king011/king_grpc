package cmd

import (
	"fmt"
	"king_grpc/version"
	"os"
	"runtime"

	"github.com/spf13/cobra"
)

const (
	// App 程式名
	App = "king_grpc"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "grpc server microservice",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(App)
			fmt.Println(runtime.GOOS, runtime.GOARCH)
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
			abort()
		}
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
