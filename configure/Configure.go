package configure

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"
	"strings"
	"sync"

	"github.com/google/go-jsonnet"
	"gitlab.com/king011/king-go/log/logger.zap"
)

const (
	// FileName 配置 檔案名稱
	FileName  = "king_grpc.jsonnet"
	_FileRoot = "fileroot"
)

var _Configure *Configure

// Configure 配置項目
type Configure struct {
	// 數據庫 配置
	DB DB
	// GRPC 服務器 配置
	GRPC GRPC
	// 日誌 配置
	Logger logger.Options

	// 檔案 root 目錄
	Fileroot string

	// 啓用的 子模塊
	Module []string

	// 子模塊 配置
	ModuleData map[string]json.RawMessage

	basePath string
	filename string
}

// BasePath 返回 程式 根目錄
func (c *Configure) BasePath() string {
	return c.basePath
}

// GetModuleData 返回模塊 配置
func (c *Configure) GetModuleData(id string) json.RawMessage {
	if len(c.ModuleData) == 0 {
		return nil
	}
	return c.ModuleData[id]
}
func (c *Configure) format() {
	c.DB.format()
	c.GRPC.format()

	c.Fileroot = strings.TrimSpace(c.Fileroot)
	if c.Fileroot == "" {
		c.Fileroot = _FileRoot
	}
	if filepath.IsAbs(c.Fileroot) {
		c.Fileroot = filepath.Clean(c.Fileroot)
	} else {
		c.Fileroot = filepath.Clean(c.basePath + "/" + c.Fileroot)
	}
}

// Init 初始化 配置
func Init(basePath, filename string) (cnf *Configure, e error) {
	cnf, e = load(basePath, filename)
	if e != nil {
		return
	}
	_Configure = cnf
	return
}
func load(basePath, filename string) (rs *Configure, e error) {
	var cnf Configure
	var b []byte
	b, e = ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	e = json.Unmarshal([]byte(jsonStr), &cnf)
	if e != nil {
		return
	}
	cnf.filename = filename
	cnf.basePath = basePath
	cnf.format()
	rs = &cnf
	return
}

var lock sync.Mutex

// Single 返回配置 單例
func Single() *Configure {
	lock.Lock()
	cnf := _Configure
	lock.Unlock()
	return cnf
}

// Reload 重載 配置
func Reload() (rs *Configure, e error) {
	cnf := Single()
	cnf, e = load(cnf.basePath, cnf.filename)
	if e != nil {
		return
	}
	lock.Lock()
	_Configure = cnf
	lock.Unlock()
	rs = cnf
	return
}
