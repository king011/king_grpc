package configure

import (
	"strings"
	"time"
)

// DB 數據庫 配置
type DB struct {
	// 數據庫 驅動
	Driver string
	// 數據庫 連接字符串
	Source string
	// 是否 顯示 SQL 指令
	ShowSQL bool
	// Ping 數據庫 時間間隔 如果 < 10 分鐘 則 不進行 ping
	Ping time.Duration
	// 數據庫 緩存 大小 如果 < 100 則 不使用 緩存
	Cache int
}

func (d *DB) format() {
	d.Driver = strings.TrimSpace(d.Driver)
	d.Source = strings.TrimSpace(d.Source)

	if d.Ping > 0 {
		d.Ping *= time.Millisecond
		if d.Ping < time.Minute*10 {
			d.Ping = 0
		}
	} else if d.Ping != 0 {
		d.Ping = 0
	}
	if d.Cache < 100 {
		d.Cache = 0
	}

}
