package configure

import (
	"strings"
	"time"
)

// GRPC 服務器 配置
type GRPC struct {
	// 服務器 工作地址
	Addr string

	// x509 證書路徑
	// 如果爲空 則使用 h2c
	CertFile string
	KeyFile  string

	// cookie 過期 時間
	Cookie int64
}

func (c *GRPC) format() {
	c.Addr = strings.TrimSpace(c.Addr)
	c.CertFile = strings.TrimSpace(c.CertFile)
	c.KeyFile = strings.TrimSpace(c.KeyFile)

	v := time.Duration(c.Cookie)
	if v > 0 {
		v *= time.Millisecond
		if v < time.Second {
			v = 0
		}
	} else if v != 0 {
		v = 0
	}
	if v == 0 {
		c.Cookie = 0
	} else {
		c.Cookie = int64(v / time.Second)
	}
}

// H2 返回是否使用 H2 模式
func (c *GRPC) H2() bool {
	return c.CertFile != "" && c.KeyFile != ""
}
