package cookie

import (
	"encoding/hex"
	"encoding/json"
	"github.com/gorilla/securecookie"
	"gitlab.com/king011/king-go/os/fileperm"
	"io/ioutil"
)

type _Key struct {
	Hash  string
	Block string
}

// Generate 創建隨機的 key
func Generate() (hashKey []byte, blockKey []byte) {
	hashKey = securecookie.GenerateRandomKey(32)
	blockKey = securecookie.GenerateRandomKey(32)
	return
}

// Save 存儲 key 到 檔案
func Save(filename string, hashKey []byte, blockKey []byte) (e error) {
	b, e := json.MarshalIndent(_Key{
		Hash:  hex.EncodeToString(hashKey),
		Block: hex.EncodeToString(blockKey),
	}, "", "\t")
	if e != nil {
		return
	}
	e = ioutil.WriteFile(filename, b, fileperm.File)
	if e != nil {
		return
	}
	return
}
