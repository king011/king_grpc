package cookie

import (
	"encoding/hex"
	"encoding/json"
	"github.com/gorilla/securecookie"
	"go.uber.org/zap"
	"io/ioutil"
	"king_grpc/logger"
	"os"
	"time"
)

var _Secure *securecookie.SecureCookie

// Encode 加密一個 cookie
func Encode(name string, value interface{}) (string, error) {
	return _Secure.Encode(name, value)
}

// Decode 解密一個 cookie
func Decode(name, value string, dst interface{}) error {
	return _Secure.Decode(name, value, dst)
}

// IsInit 返回 是否 初始化
func IsInit() bool {
	return _Secure != nil
}

// IniClient 以 客戶端 測試模式 初始化
func IniClient(filename string) (e error) {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		return
	}

	var key _Key
	e = json.Unmarshal(b, &key)
	if e != nil {
		return
	}
	hashKey, e := hex.DecodeString(key.Hash)
	if e != nil {
		return
	}
	blockKey, e := hex.DecodeString(key.Block)
	if e != nil {
		return
	}

	_Secure = securecookie.New(hashKey, blockKey)
	return
}

// Init 初始化
func Init(filename string, maxAge int64) {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		if os.IsNotExist(e) {
			newGenerate(filename)
			return
		}
		if ce := logger.Logger.Check(zap.FatalLevel, "load securecookie"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	var key _Key
	e = json.Unmarshal(b, &key)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "unmarshal securecookie"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	hashKey, e := hex.DecodeString(key.Hash)
	if e != nil {
		os.Exit(1)
	} else if len(hashKey) != 32 {
		if ce := logger.Logger.Check(zap.FatalLevel, "bad hash"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.String("key", key.Hash),
			)
		}
		os.Exit(1)
	}
	blockKey, e := hex.DecodeString(key.Block)
	if e != nil {
		os.Exit(1)
	} else if len(blockKey) != 32 {
		if ce := logger.Logger.Check(zap.FatalLevel, "bad block"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.String("key", key.Block),
			)
		}
		os.Exit(1)
	}
	_Secure = securecookie.New(hashKey, blockKey)
	_Secure.MaxAge((int)(maxAge))
	if ce := logger.Logger.Check(zap.InfoLevel, "cookie"); ce != nil {
		ce.Write(
			logger.GroupSystem,
			zap.String("timeout", (time.Duration(maxAge)*time.Second).String()),
		)
	}
	return
}
func newGenerate(filename string) {
	hashKey, blockKey := Generate()
	e := Save(filename, hashKey, blockKey)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "save securecookie"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	_Secure = securecookie.New(hashKey, blockKey)
}
