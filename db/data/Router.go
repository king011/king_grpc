package data

// Router 定義了 grpc 接口 需要的 權限
type Router struct {
	// 唯一 id
	ID int64 `xorm:"pk autoincr 'id'"`

	// 父id/分組id 爲 ui 程式 提供了 一個 參考用的 分組 不影響服務器 路由 判斷
	PID int64 `xorm:"index notnull 'pid' default 0"`

	// 爲 ui 程式 提供了 一個 參考用的 顯示 名稱
	Name string `xorm:"notnull default ''"`

	// 授權值
	Val int32 `xorm:"unique notnull default 0"`

	// 此權限能夠訪問的 路由
	Rule string `xorm:"TEXT notnull"`
}
