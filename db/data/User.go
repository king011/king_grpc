package data

import (
	"king_grpc/cookie"
	"king_grpc/utils"
	"time"
)

const (
	// UserStatusNil 註冊 未激活
	UserStatusNil = 1
	// UserStatusActive 已經激活
	UserStatusActive = 2
	// UserStatusDisable 禁用
	UserStatusDisable = 3
)

// User 定義了 訪問的 用戶信息
type User struct {
	// 唯一 id
	ID int64 `xorm:"pk autoincr 'id'"`

	// 用戶權限 以 ` 分隔多個權限
	Power string `xorm:"TEXT notnull"`

	// 用戶登入名稱
	Name string `xorm:"unique notnull"`

	// 用戶登入密碼 sha512
	Password string `xorm:"notnull"`

	// 用戶 狀態
	Status uint32 `xorm:"index notnull"`
}

// ToSession 轉爲 session
func (d *User) ToSession() (clone *cookie.Session) {
	clone = &cookie.Session{
		ID:    d.ID,
		Name:  d.Name,
		Power: utils.SplitInt32(d.Power),
		Time:  time.Now(),
	}
	return
}
