package manipulator

import (
	"errors"
	"fmt"
	"king_grpc/db/data"
	"king_grpc/logger"
	"regexp"
	"sort"
	"strings"
	"sync"

	"go.uber.org/zap"
)

const (
	_SortEqual = 10 + iota
	_SortStart
	_SortEnd
	_SortRegexp
)

var errRuleEmpty = errors.New("rule empty")
var _Router _RouterImpl

type _RouterImpl struct {
	sync.RWMutex
	// 規則
	rule _RuleImpl
	// 緩存 規則定義
	items []data.Router
}

type _IRule interface {
	Match(url string) bool
	Val() int32
	Rule() string
	Name() string
	Sort() int
}
type _RuleEqual struct {
	val   int32
	items map[string]bool
	name  string
}

func (r *_RuleEqual) Match(url string) bool {
	return r.items[url]
}
func (r *_RuleEqual) Val() int32 {
	return r.val
}
func (r *_RuleEqual) Name() string {
	return r.name
}
func (r *_RuleEqual) Rule() string {
	n := len(r.items)
	if n == 0 {
		return "[]"
	}
	strs := make([]string, 0, n)
	for k := range r.items {
		strs = append(strs, k)
	}
	return fmt.Sprintf("=: %v", strs)
}
func (r *_RuleEqual) Sort() int {
	return _SortEqual
}

type _RuleRegexp struct {
	val   int32
	match *regexp.Regexp
	name  string
}

func (r *_RuleRegexp) Match(url string) bool {
	return r.match.MatchString(url)
}
func (r *_RuleRegexp) Val() int32 {
	return r.val
}
func (r *_RuleRegexp) Name() string {
	return r.name
}
func (r *_RuleRegexp) Rule() string {
	return fmt.Sprintf("%%: %v", r.match)
}
func (r *_RuleRegexp) Sort() int {
	return _SortRegexp
}

type _RuleWidth struct {
	val   int32
	items []string
	name  string
}

func (r *_RuleWidth) Val() int32 {
	return r.val
}
func (r *_RuleWidth) Name() string {
	return r.name
}

type _RuleStartWidth struct {
	_RuleWidth
}

func (r *_RuleStartWidth) Match(url string) bool {
	for i := 0; i < len(r.items); i++ {
		if strings.HasPrefix(url, r.items[i]) {
			return true
		}
	}
	return false
}

func (r *_RuleStartWidth) Sort() int {
	return _SortStart
}
func (r *_RuleStartWidth) Rule() string {
	if r.items == nil {
		return "[]"
	}
	return fmt.Sprintf("s: %v", r.items)
}

type _RuleEndWidth struct {
	_RuleWidth
}

func (r *_RuleEndWidth) Match(url string) bool {
	for i := 0; i < len(r.items); i++ {
		if strings.HasSuffix(url, r.items[i]) {
			return true
		}
	}
	return false
}
func (r *_RuleEndWidth) Sort() int {
	return _SortEnd
}
func (r *_RuleEndWidth) Rule() string {
	if r.items == nil {
		return "[]"
	}
	return fmt.Sprintf("e: %v", r.items)
}

type _RuleImpl struct {
	m map[int32]_IRule
}

func (r *_RuleImpl) AppendEqual(val int32, name, str string) (e error) {
	str = strings.TrimSpace(str)
	if str == "" {
		e = errRuleEmpty
		return
	}
	items := make(map[string]bool)
	strs := strings.Split(str, ":")
	for i := 0; i < len(strs); i++ {
		str = strings.TrimSpace(strs[i])
		if str == "" {
			continue
		}
		items[str] = true
	}
	if len(items) == 0 {
		e = errRuleEmpty
		return
	}

	if r.m == nil {
		r.m = make(map[int32]_IRule)
	}
	r.m[val] = &_RuleEqual{
		val:   val,
		name:  name,
		items: items,
	}
	return
}
func (r *_RuleImpl) AppendWidth(val int32, name, str string, start bool) (e error) {
	str = strings.TrimSpace(str)
	if str == "" {
		e = errRuleEmpty
		return
	}
	strs := strings.Split(str, ":")
	items := make([]string, 0, len(strs))
	for i := 0; i < len(strs); i++ {
		str = strings.TrimSpace(strs[i])
		if str == "" {
			continue
		}
		items = append(items, str)
	}
	if len(items) == 0 {
		e = errRuleEmpty
		return
	}
	var rule _IRule
	if start {
		rule = &_RuleStartWidth{
			_RuleWidth{
				val:   val,
				name:  name,
				items: items,
			},
		}
	} else {
		rule = &_RuleEndWidth{
			_RuleWidth{
				val:   val,
				name:  name,
				items: items,
			},
		}
	}
	if r.m == nil {
		r.m = make(map[int32]_IRule)
	}
	r.m[val] = rule
	return
}

func (r *_RuleImpl) AppendRegexp(val int32, name, str string) (e error) {
	str = strings.TrimSpace(str)
	if str == "" {
		e = errRuleEmpty
		return
	}
	var match *regexp.Regexp
	match, e = regexp.Compile(str)
	if e != nil {
		return
	}

	if r.m == nil {
		r.m = make(map[int32]_IRule)
	}
	r.m[val] = &_RuleRegexp{
		val:   val,
		name:  name,
		match: match,
	}
	return
}
func (r *_RuleImpl) Sort() []_IRule {
	items := make([]_IRule, 0, len(r.m))
	for _, v := range r.m {
		items = append(items, v)
	}
	sort.Sort((_ISortRule)(items))
	return items
}

type _ISortRule []_IRule

// Len is the number of elements in the collection.
func (arrs _ISortRule) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs _ISortRule) Less(i, j int) bool {
	return arrs[i].Sort() < arrs[j].Sort()
}

// Swap swaps the elements with indexes i and j.
func (arrs _ISortRule) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}
func loadRule() (items []data.Router, rule _RuleImpl, err error) {
	err = Engine().Find(&items)
	if err != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "Router.Init"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(err),
			)
		}
		return
	} else if len(items) == 0 {
		return
	}
	var e error
	for _, item := range items {
		str := strings.ToLower(strings.TrimSpace(item.Rule))
		if str == "" {
			continue
		} else if strings.HasPrefix(str, "=:") {
			e = rule.AppendEqual(item.Val, item.Name, str[2:])
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "rule =:"); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Int64("id", item.ID),
						zap.String("name", item.Name),
						zap.Int32("val", item.Val),
						zap.String("rule", str),
					)
				}
			}
		} else if strings.HasPrefix(str, "s:") {
			e = rule.AppendWidth(item.Val, item.Name, str[2:], true)
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "rule s:"); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Int64("id", item.ID),
						zap.String("name", item.Name),
						zap.Int32("val", item.Val),
						zap.String("rule", str),
					)
				}
			}
		} else if strings.HasPrefix(str, "e:") {
			e = rule.AppendWidth(item.Val, item.Name, str[2:], false)
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "rule e:"); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Int64("id", item.ID),
						zap.String("name", item.Name),
						zap.Int32("val", item.Val),
						zap.String("rule", str),
					)
				}
			}
		} else if strings.HasPrefix(str, "%:") {
			e = rule.AppendRegexp(item.Val, item.Name, str[2:])
			if e != nil {
				if ce := logger.Logger.Check(zap.WarnLevel, "rule %:"); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Int64("id", item.ID),
						zap.String("name", item.Name),
						zap.Int32("val", item.Val),
						zap.String("rule", str),
					)
				}
			}
		} else {
			if ce := logger.Logger.Check(zap.WarnLevel, "rule unknow"); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Int64("id", item.ID),
					zap.String("name", item.Name),
					zap.Int32("val", item.Val),
					zap.String("rule", str),
				)
			}
		}
	}

	return
}
func (m *_RouterImpl) Reload() (e error) {
	items, rule, e := loadRule()
	if e != nil {
		return
	}
	if len(rule.m) != 0 {
		if ce := logger.Logger.Check(zap.InfoLevel, "rule"); ce != nil {
			items := rule.Sort()
			for _, item := range items {
				if ce == nil {
					ce = logger.Logger.Check(zap.InfoLevel, "rule")
				}
				if ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.String("name", item.Name()),
						zap.Int32("val", item.Val()),
						zap.String("rule", item.Rule()),
					)
					ce = nil
				}
			}
		}
	}
	_Router.Lock()
	_Router.rule = rule
	_Router.items = items
	_Router.Unlock()
	return
}

// RouterMatch 返回 是否有權限 訪問 url
func RouterMatch(url string, vals ...int32) bool {
	return _Router.Match(url, vals...)
}
func (m *_RouterImpl) Match(url string, vals ...int32) (yes bool) {
	m.RLock()
	keys := m.rule.m
	m.RUnlock()
	rules := make([]_IRule, 0, len(vals)+1)
	if rule, ok := keys[0]; ok {
		rules = append(rules, rule)
	}
	for _, val := range vals {
		if val == 0 {
			continue
		}
		if rule, ok := keys[val]; ok {
			rules = append(rules, rule)
		}
	}
	if len(rules) == 0 {
		return
	} else if len(rules) > 1 {
		sort.Sort(_ISortRule(rules))
	}
	for _, rule := range rules {
		yes = rule.Match(url)
		if yes {
			return
		}
	}
	return
}
