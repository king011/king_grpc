package manipulator

import (
	"errors"
	"fmt"
	"king_grpc/db/data"
	"king_grpc/logger"

	"go.uber.org/zap"
)

var errNamePasswordNotMatch = errors.New("name or password not matched")

// User 定義了 訪問的 用戶信息
type User struct {
}

func (m User) tagLogin() string {
	return "db/manipulator/User.Login"
}

// Login 用戶登入
func (m User) Login(name, password string) (user *data.User, e error) {
	if name == "" {
		e = errNamePasswordNotMatch
		return
	}
	bean := &data.User{
		Name: name,
	}
	ok, e := Engine().Get(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLogin()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	} else if !ok || bean.Password != password {
		e = errNamePasswordNotMatch
		return
	} else if bean.Status != data.UserStatusActive {
		e = fmt.Errorf("user not active,status = %v", bean.Status)
		return
	}

	user = bean
	return
}
func (m User) tagGetByID() string {
	return "db/manipulator/User.GetByID"
}

// GetByID 依據 id 返回 用戶 信息
func (m User) GetByID(id int64) (user *data.User, e error) {
	if id == 0 {
		return
	}
	bean := &data.User{
		ID: id,
	}
	var ok bool
	if ok, e = Engine().Get(&bean); e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagGetByID()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	} else if ok && bean.Status == data.UserStatusActive {
		user = bean
	}
	return
}
func (m User) tagPassword() string {
	return "db/manipulator/User.Password"
}

// Password 修改登入密碼
func (m User) Password(id int64, password string) (rows int64, e error) {
	if id == 0 || password == "" {
		return
	}
	rows, e = Engine().ID(id).Update(&data.User{
		Password: password,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagPassword()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	return
}
