package manipulator

import (
	"github.com/go-xorm/xorm"

	"king_grpc/configure"
	"king_grpc/db/data"
	"king_grpc/logger"
	"os"
	"time"

	"go.uber.org/zap"
)

var _Engine *xorm.Engine

// Init 初始化 設置
func Init() {
	cnf := configure.Single()

	// 連接數據庫
	engine, e := xorm.NewEngine(cnf.DB.Driver, cnf.DB.Source)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "NewEngine"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	e = engine.Ping()
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "Ping"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// 初始化 數據庫 設置
	if cnf.DB.ShowSQL {
		engine.ShowSQL(true)
		if ce := logger.Logger.Check(zap.DebugLevel, "set db option"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Bool("ShowSQL", true),
			)
		}
	}
	// 初始化 緩存
	if cnf.DB.Cache != 0 {
		cacher := xorm.NewLRUCacher(xorm.NewMemoryStore(), cnf.DB.Cache)
		engine.SetDefaultCacher(cacher)
	}

	// 初始化 數據庫
	beans := []interface{}{
		&data.Router{},
		&data.User{},
	}
	session := engine.NewSession()
	e = session.Begin()
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "begin"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	for _, bean := range beans {
		e = InitDB(session, bean)
		if e != nil {
			break
		}
	}
	if e == nil {
		session.Commit()
		session.Close()
	} else {
		session.Rollback()
		session.Close()
		if ce := logger.Logger.Check(zap.FatalLevel, "initDB"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// ping
	if cnf.DB.Ping > 0 {
		go func() {
			var e error
			for {
				time.Sleep(cnf.DB.Ping)
				e = engine.Ping()
				if e != nil {
					if ce := logger.Logger.Check(zap.ErrorLevel, "Ping"); ce != nil {
						ce.Write(
							logger.GroupDB,
							zap.Error(e),
						)
					}
				}
			}
		}()
	}
	_Engine = engine
	if ce := logger.Logger.Check(zap.InfoLevel, "connected db"); ce != nil {
		ce.Write(
			logger.GroupDB,
		)
	}

	e = _Router.Reload()
	if e != nil {
		os.Exit(1)
	}

}

// RouterReload 重載 路由配置
func RouterReload() error {
	return _Router.Reload()
}

// RouterItems 返回 當前 路由配置
func RouterItems() (items []data.Router) {
	_Router.Lock()
	if len(_Router.items) == 0 {
		_Router.Unlock()
		return
	}
	items = make([]data.Router, len(_Router.items))
	copy(items, _Router.items)
	_Router.Unlock()
	return
}

// InitDB create table
func InitDB(session *xorm.Session, bean interface{}) (e error) {
	var exist bool
	exist, e = session.IsTableExist(bean)
	if e != nil {
		return
	}
	if !exist {
		e = session.CreateTable(bean)
		if e != nil {
			return
		}
		e = session.CreateUniques(bean)
		if e != nil {
			return
		}
		e = session.CreateIndexes(bean)
		if e != nil {
			return
		}
	}
	return
}

// Engine 返回 數據庫 引擎
func Engine() *xorm.Engine {
	return _Engine
}

// Session 創建一個 數據庫 session
func Session() *xorm.Session {
	return _Engine.NewSession()
}

// Transaction 啓動一個 數據庫 事務
func Transaction() (session *xorm.Session, e error) {
	session = _Engine.NewSession()
	e = session.Begin()
	if e != nil {
		session.Close()
		session = nil
	}
	return
}

// CloseTransaction 提交或取消 數據庫 操作
func CloseTransaction(session *xorm.Session, e *error) {
	if *e == nil {
		session.Commit()
	} else {
		session.Rollback()
	}
	session.Close()
}

type _Table string

func (t _Table) TableName() string {
	return string(t)
}

// ClearCache 清除 數據庫 緩存
func ClearCache(names ...string) (e error) {
	size := len(names)
	if size == 0 {
		return
	}
	switch size {
	case 0:
	case 1:
		e = Engine().ClearCache(
			_Table(names[0]),
		)
	case 2:
		e = Engine().ClearCache(
			_Table(names[0]),
			_Table(names[1]),
		)
	case 3:
		e = Engine().ClearCache(
			_Table(names[0]),
			_Table(names[1]),
			_Table(names[2]),
		)
	default:
		beans := make([]interface{}, size)
		for i := 0; i < size; i++ {
			beans[i] = _Table(names[i])
		}
		e = Engine().ClearCache(beans...)
	}

	return
}
