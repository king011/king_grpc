module king_grpc

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/xorm v0.7.1
	github.com/golang/protobuf v1.3.1
	github.com/google/go-jsonnet v0.12.1
	github.com/gorilla/securecookie v1.1.1
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	gitlab.com/king011/king-go v0.0.3
	go.uber.org/zap v1.9.1
	google.golang.org/grpc v1.19.1
)
