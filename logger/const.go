package logger

import (
	"go.uber.org/zap"
)

// GroupSystem 系統/框架 api 返回 信息
var GroupSystem = zap.String("LogGroup", "system")

// GroupGRPC grpc 返回的信息
var GroupGRPC = zap.String("LogGroup", "grpc")

// GroupIO io 相關
var GroupIO = zap.String("LogGroup", "io")

// GroupDB 數據庫 相關
var GroupDB = zap.String("LogGroup", "db")

// GroupSession 用戶登入/登出 相關
var GroupSession = zap.String("LogGroup", "session")

// GroupAction API 信息
var GroupAction = zap.String("LogGroup", "action")

// Success 寫入 成功 信息
var Success = zap.Bool("Success", true)
