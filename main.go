package main

import (
	_ "king_grpc/module/public"
	_ "king_grpc/module/server_list"
	_ "king_grpc/module/session"
	_ "king_grpc/module/shared"
	_ "king_grpc/module/simple_file"
	_ "king_grpc/module/system"
	_ "king_grpc/module/user"

	_ "github.com/go-sql-driver/mysql"

	"king_grpc/cmd"
)

func main() {
	cmd.Execute()
}
