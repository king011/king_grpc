package module

import (
	"context"

	king_io "gitlab.com/king011/king-go/io"
	"google.golang.org/grpc"
)

// Client 定義 客戶端 接口
type Client interface {
	// user io
	IO() *king_io.InputReader

	// SetCookie set cookie
	SetCookie(string)
	// GetCookie get cookie
	GetCookie() string
	// GetConn get grpc conn
	GetConn() *grpc.ClientConn

	// Background return background context
	Background() context.Context
}
