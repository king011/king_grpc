package module

import (
	"errors"
	"fmt"
	"king_grpc/configure"
	"king_grpc/logger"
	"strings"
	"sync"

	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var _Single = &Management{
	module: make(map[string]Module),
}

// Single 返回 模塊管理器 單件
func Single() *Management {
	return _Single
}

// Management 負責管理 模塊
type Management struct {
	module map[string]Module
	enable []Module
	sync.Mutex
}

// ID 返回 所有 啓用模塊的 id
func (m *Management) ID() (arrs []string) {
	m.Lock()
	defer m.Unlock()
	if len(m.enable) != 0 {
		arrs = make([]string, len(m.enable))
		for i := 0; i < len(m.enable); i++ {
			arrs[i] = m.enable[i].ID()
		}
	}
	return arrs
}

// Register 註冊模塊
//
// 所有子模板 必須調用此函數 註冊 自己
func (m *Management) Register(module Module) {
	m.Lock()
	defer m.Unlock()

	id := module.ID()
	if _, ok := m.module[id]; ok {
		panic(fmt.Sprint("module already exists :", id))
	}
	m.module[id] = module
}

// OnStart 爲所有啓用的 子模塊 調用 OnStart
func (m *Management) OnStart() {
	m.Lock()
	defer m.Unlock()

	module := m.enable
	cnf := configure.Single()
	for i := 0; i < len(module); i++ {
		id := module[i].ID()
		module[i].OnStart(cnf.BasePath(), cnf.GetModuleData(id))
	}
}

// RegisterGRPC 爲所有啓用的 子模塊 調用 RegisterGRPC
func (m *Management) RegisterGRPC(srv *grpc.Server) {
	m.Lock()
	defer m.Unlock()

	module := m.enable
	for i := 0; i < len(module); i++ {
		module[i].RegisterGRPC(srv)
	}
}

// OnStop 爲所有啓用的 子模塊 調用 OnStop
func (m *Management) OnStop() {
	m.Lock()
	defer m.Unlock()

	module := m.enable
	for i := 0; i < len(module); i++ {
		module[i].OnStop()
	}
}

// Enable 啓用子模塊
func (m *Management) Enable(modules []string) {
	m.Lock()
	defer m.Unlock()

	keys := make(map[string]bool)

	for i := 0; i < len(modules); i++ {
		id := modules[i]
		if keys[id] {
			continue
		}
		keys[id] = true

		if module, ok := m.module[id]; ok {
			m.enable = append(m.enable, module)
			if ce := logger.Logger.Check(zap.InfoLevel, "enable module"); ce != nil {
				ce.Write(
					zap.String("ID", id),
				)
			}
		} else {
			if ce := logger.Logger.Check(zap.WarnLevel, "module not support"); ce != nil {
				ce.Write(
					zap.String("ID", id),
				)
			}
		}
	}
}

// Client 返回所有支持 客戶端 操作的模塊
func (m *Management) Client() []string {
	m.Lock()
	defer m.Unlock()

	arrs := make([]string, 0, len(m.module))
	for id, module := range m.module {
		if nil != module.Client() {
			arrs = append(arrs, id)
		}
	}
	return arrs
}

// OnClient 執行客戶端 操作
func (m *Management) OnClient(client Client, id string) bool {
	m.Lock()
	defer m.Unlock()

	module := m.module[id]
	if module != nil {
		c := module.Client()
		if c != nil {
			return c.OnClient(client)
		}
		client.IO().Eprintln("module not support client :", id)
	}
	client.IO().Eprintln("not found module :", id)
	return false
}

// ClearDBCache 通知 模塊 清空 數據庫 緩存
//
// 如果 id 爲空則 清空 所有模塊 緩存
func (m *Management) ClearDBCache(id, tag string) (e error) {
	m.Lock()
	defer m.Unlock()

	if id == "" {
		var err []string
		for i := 0; i < len(m.enable); i++ {
			e = m.enable[i].OnClearDBCache(tag)
			if e != nil {
				err = append(err, fmt.Sprintf("%v:%v", m.enable[i].ID(), e))
			}
		}
		if len(err) != 0 {
			e = errors.New(
				strings.Join(err, ";"),
			)
		}
	} else {
		for i := 0; i < len(m.enable); i++ {
			if m.enable[i].ID() != id {
				continue
			}
			e = m.enable[i].OnClearDBCache(tag)
			break
		}
	}
	return
}

// ClearCache 通知 模塊 清空 緩存
//
// 如果 id 爲空則 清空 所有模塊 緩存
func (m *Management) ClearCache(id, tag string) (e error) {
	m.Lock()
	defer m.Unlock()

	if id == "" {
		var err []string
		for i := 0; i < len(m.enable); i++ {
			e = m.enable[i].OnClearCache(tag)
			if e != nil {
				err = append(err, fmt.Sprintf("%v:%v", m.enable[i].ID(), e))
			}
		}
		if len(err) != 0 {
			e = errors.New(
				strings.Join(err, ";"),
			)
		}
	} else {
		for i := 0; i < len(m.enable); i++ {
			if m.enable[i].ID() != id {
				continue
			}
			e = m.enable[i].OnClearCache(tag)
			break
		}
	}
	return
}

// Reload 通知 模塊 重載配置
//
// 如果 id 爲空則 清空 所有模塊 重載
func (m *Management) Reload(id, tag string) (e error) {
	m.Lock()
	defer m.Unlock()

	cnf, e := configure.Reload()
	if e != nil {
		return
	}
	if id == "" {
		var err []string
		for i := 0; i < len(m.enable); i++ {
			id := m.enable[i].ID()
			e = m.enable[i].OnReload(cnf.BasePath(), cnf.GetModuleData(id), tag)
			if e != nil {
				err = append(err, fmt.Sprintf("%v:%v", id, e))
			}
		}
		if len(err) != 0 {
			e = errors.New(
				strings.Join(err, ";"),
			)
		}
	} else {
		for i := 0; i < len(m.enable); i++ {
			if m.enable[i].ID() != id {
				continue
			}
			e = m.enable[i].OnReload(cnf.BasePath(), cnf.GetModuleData(id), tag)
			break
		}
	}
	return
}
