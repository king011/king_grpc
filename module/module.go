// Package module 爲子模塊 每個 檔案夾 爲一個子模塊 實現的 特定的 功能
//
// 每個子模塊應該 實現一個 New 方法 返回 Module 接口
package module

import (
	"encoding/json"

	"google.golang.org/grpc"
)

// Module 定義了子模塊 接口
type Module interface {
	// ID 返回 唯一的子模塊 名稱
	ID() string

	// OnStart 初始化 回調 應該在此 執行 模塊 初始化
	OnStart(basePath string, data json.RawMessage)

	// RegisterGRPC 爲子模塊 註冊 grpc 服務
	RegisterGRPC(srv *grpc.Server)

	// OnReload 模塊重載配置 回調
	OnReload(basePath string, data json.RawMessage, tag string) (e error)

	// 通知 子模塊 清空 數據庫 緩存
	//
	// tag 是一個 自定義標記 通常是 表名 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearDBCache(tag string) error

	// 通知 子模塊 清空 緩存
	//
	// tag 是一個 自定義標記 通常是 子模塊 依據此值來確定 要清除那些緩存
	//
	// 通常 tag 如果爲 空字符串 則應該 清除 全部 緩存
	OnClearCache(tag string) error

	// OnStop 停止 回調 應該在此 執行 資源釋放 和 停止模塊工作
	OnStop()

	// Client 返回 客戶端 接口
	//
	// 如果返回 nil 則 不支持 客戶端
	Client() OnClient
}

// OnClient 執行 客戶端 操作
type OnClient interface {
	// OnClient 執行 客戶端 操作
	//
	// 返回 ture 將 退出 程式
	OnClient(client Client) bool
}

// Base 爲 子模塊 實現了 默認的 方法 通常 應該 將 ModuleBase 嵌入到 子模塊中
type Base struct {
}

// OnStart .
func (Base) OnStart(basePath string, data json.RawMessage) {
}

// RegisterGRPC .
func (Base) RegisterGRPC(srv *grpc.Server) {
}

// OnClearDBCache .
func (Base) OnClearDBCache(tag string) error {
	return nil
}

// OnClearCache .
func (Base) OnClearCache(tag string) error {
	return nil
}

// OnStop .
func (Base) OnStop() {

}

// Client .
func (Base) Client() OnClient {
	return nil
}

// OnReload .
func (Base) OnReload(basePath string, data json.RawMessage, tag string) (e error) {
	return
}
