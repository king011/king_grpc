package public

import (
	"king_grpc/module"
	grpc_public "king_grpc/protocol/module/public"
	"king_grpc/utils"
	"time"

	king_io "gitlab.com/king011/king-go/io"
)

type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "ping",
			Usage: "ping server",
			Done:  c.ping,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}

func (c *_Client) ping() bool {
	last := time.Now()
	client := grpc_public.NewServiceClient(c.client.GetConn())
	_, e := client.Ping(c.client.Background(),
		&grpc_public.PingRequest{},
	)
	if e == nil {
		c.Println("ping", time.Now().Sub(last))
	} else {
		c.Eprintln(e, time.Now().Sub(last))
	}
	return false
}
