package public

import (
	"context"

	grpc_public "king_grpc/protocol/module/public"
)

type _Impl struct {
}

func (s _Impl) tagPing() string {
	return "module/public Impl.Ping"
}

var pingResponse grpc_public.PingResponse

func (s _Impl) Ping(ctx context.Context, request *grpc_public.PingRequest) (response *grpc_public.PingResponse, e error) {
	response = &pingResponse
	return
}
