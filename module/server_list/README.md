# 模塊信息

id : server_list

此模塊 主要 提供了 服務器 連接信息查詢的 服務

客戶端傳來 服務器 編碼 返回 編碼關聯的服務器基本信息

# 路由和權限

insert router (name,val,rule) values 
    ("public",0,"s:/module.server_list.service/get"), 

insert router (name,val,rule) values 
    ("server_list",220,"s:/module.server_list.service/"),

# 協議

pb/king_grpc/protocol/module/server_list

# 佔用數據庫

module_server_list

# 佔用 檔案/檔案夾

無