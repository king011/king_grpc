package list

import (
	grpc_server_list "king_grpc/protocol/module/server_list"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// FindArgs 查詢參數 定義
type FindArgs struct {
	// 查詢模式
	Mode grpc_server_list.FindMode
	// 查詢 條件 如果爲空 不限定 條件
	Name     string
	Code     string
	Addr     string
	Protocol string

	mode string
}

// FromPB .
func (args *FindArgs) FromPB(clone *grpc_server_list.FindArgs) (e error) {
	if clone == nil {
		args.Reset()
		return
	}
	if clone.Name == "" &&
		clone.Code == "" &&
		clone.Addr == "" &&
		clone.Protocol == "" {
		args.Reset()
		return
	}
	switch clone.Mode {
	case grpc_server_list.FindMode_Equal:
		args.mode = "="
	case grpc_server_list.FindMode_Like:
		args.mode = "like"
	case grpc_server_list.FindMode_Regexp:
		args.mode = "regexp"
	default:
		e = status.Errorf(codes.InvalidArgument, "not support find mode : %v", clone.Mode)
		return
	}

	args.Name = clone.Name
	args.Code = clone.Code
	args.Addr = clone.Addr
	args.Protocol = clone.Protocol
	return
}

// Reset .
func (args *FindArgs) Reset() {
	if args.Name != "" {
		args.Name = ""
	}
	if args.Code != "" {
		args.Code = ""
	}
	if args.Addr != "" {
		args.Addr = ""
	}
	if args.Protocol != "" {
		args.Protocol = ""
	}
}
