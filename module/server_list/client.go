package list

import (
	"encoding/json"
	"king_grpc/module"
	grpc_server_list "king_grpc/protocol/module/server_list"
	"king_grpc/utils"

	king_io "gitlab.com/king011/king-go/io"
)

type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "get",
			Usage: "get server info by code",
			Done:  c.get,
		},
		utils.Command{
			Name:  "add",
			Usage: "add a new server",
			Done:  c.add,
		},
		utils.Command{
			Name:  "update",
			Usage: "update server info",
			Done:  c.update,
		},
		utils.Command{
			Name:  "remove",
			Usage: "remove server",
			Done:  c.remove,
		},
		utils.Command{
			Name:  "count",
			Usage: "count server",
			Done:  c.count,
		},
		utils.Command{
			Name:  "find",
			Usage: "find server",
			Done:  c.find,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}
func (c *_Client) printJSON(v interface{}) {
	b, e := json.MarshalIndent(v, "", "\t")
	if e == nil {
		c.Println(string(b))
	} else {
		c.Eprintln(e)
	}
}
func (c *_Client) get() bool {
	code := c.ReadString("input server code")
	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	response, e := client.Get(c.client.Background(),
		&grpc_server_list.GetRequest{
			Code: code,
		},
	)
	if e == nil {
		c.printJSON(response.Data)
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) add() bool {
	data := c.inputData()
	if data == nil {
		return false
	}

	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	_, e := client.Add(c.client.Background(),
		&grpc_server_list.AddRequest{
			Data: data,
		},
	)
	if e == nil {
		c.Println("new success")
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) inputData() (rs *grpc_server_list.Server) {
	data := &grpc_server_list.Server{}
	for {
		data.Code = c.ReadString("input code or c<cancel>")
		if data.Code == "c" {
			return
		}
		data.Name = c.ReadString("input name or c<cancel>")
		if data.Name == "c" {
			return
		}
		data.Addr = c.ReadString("input addr or c<cancel>")
		if data.Addr == "c" {
			return
		}
		data.Protocol = c.ReadString("input protocol or c<cancel>")
		if data.Protocol == "c" {
			return
		}
		for {
			c.printJSON(data)
			cmd := c.ReadString("s<sure> or c<cancal> or r<reset>")
			if cmd == "s" {
				rs = data
				return
			} else if cmd == "c" {
				return
			} else if cmd == "r" {
				break
			}
		}
	}
}
func (c *_Client) update() bool {
	data := c.inputData()
	if data == nil {
		return false
	}

	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	response, e := client.Update(c.client.Background(),
		&grpc_server_list.UpdateRequest{
			Data: data,
		},
	)
	if e == nil {
		if response.Rows == 0 {
			c.Eprintln("not update server :", data.Code)
		} else {
			c.Println("update success")
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) remove() bool {
	code := c.ReadString("input server code")

	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	response, e := client.Remove(c.client.Background(),
		&grpc_server_list.RemoveRequest{
			Code: code,
		},
	)
	if e == nil {
		if response.Rows == 0 {
			c.Eprintln("not found server :", code)
		} else {
			c.Println("remove success")
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) count() bool {
	args := c.inputFind()
	if args == nil {
		return false
	}

	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	response, e := client.Count(c.client.Background(),
		&grpc_server_list.CountRequest{
			Args: args,
		},
	)
	if e == nil {
		c.Println("count =", response.Count)
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) inputFind() (rs *grpc_server_list.FindArgs) {
	data := &grpc_server_list.FindArgs{}
	for {
		for {
			mode := c.ReadString("input mode =<equal> *<like> r<regexp> or c<cancel>")
			if mode == "c" {
				return
			} else if mode == "=" {
				data.Mode = grpc_server_list.FindMode_Equal
				break
			} else if mode == "*" {
				data.Mode = grpc_server_list.FindMode_Like
				break
			} else if mode == "r" {
				data.Mode = grpc_server_list.FindMode_Regexp
				break
			}
		}

		data.Code = c.ReadStringDefault("input code or s<sure> or c<cancel>", "")
		if data.Code == "c" {
			return
		}
		if data.Code == "s" {
			data.Code = ""
		}

		data.Name = c.ReadStringDefault("input name or s<sure> or c<cancel>", "")
		if data.Name == "c" {
			return
		}
		if data.Name == "s" {
			data.Name = ""
		}

		data.Addr = c.ReadStringDefault("input addr or s<sure> or c<cancel>", "")
		if data.Addr == "c" {
			return
		}
		if data.Addr == "s" {
			data.Addr = ""
		}

		data.Protocol = c.ReadStringDefault("input protocol or s<sure> or c<cancel>", "")
		if data.Protocol == "c" {
			return
		}
		if data.Protocol == "s" {
			data.Protocol = ""
		}

		for {
			c.printJSON(data)
			cmd := c.ReadStringDefault("s<sure> or c<cancel> or r<reset>", "")
			if cmd == "s" {
				rs = data
				return
			} else if cmd == "c" {
				return
			} else if cmd == "r" {
				break
			}
		}
	}
}
func (c *_Client) find() bool {
	args := c.inputFind()
	if args == nil {
		return false
	}
	var limit int64 = -1
	for limit < 0 {
		limit = c.ReadNumber("input limit")
	}
	var start int64 = -1
	for start < 0 {
		start = c.ReadNumber("input start")
	}

	client := grpc_server_list.NewServiceClient(c.client.GetConn())
	response, e := client.Find(c.client.Background(),
		&grpc_server_list.FindRequest{
			Args:  args,
			Limit: limit,
			Start: start,
		},
	)
	if e == nil {
		c.printJSON(response.Data)
	} else {
		c.Eprintln(e)
	}
	return false
}
