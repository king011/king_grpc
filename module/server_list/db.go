package list

import (
	"errors"
	"fmt"
	"king_grpc/db/manipulator"
	"king_grpc/logger"

	"github.com/go-xorm/xorm"

	grpc_server_list "king_grpc/protocol/module/server_list"

	"go.uber.org/zap"
)

const (
	// ColCode .
	ColCode = "code"
	// ColName .
	ColName = "name"
	// ColAddr .
	ColAddr = "addr"
	// ColProtocol .
	ColProtocol = "protocol"
	// TableName .
	TableName = "module_server_list"
)

// Server 服務器 信息
type Server struct {
	Code     string `xorm:"varchar(50) notnull pk"`
	Name     string `xorm:"notnull"`
	Addr     string `xorm:"notnull"`
	Protocol string `xorm:"notnull"`
}

// TableName .
func (Server) TableName() string {
	return TableName
}

// FromPB .
func (s *Server) FromPB(clone *grpc_server_list.Server) {
	s.Code = clone.Code
	s.Name = clone.Name
	s.Addr = clone.Addr
	s.Protocol = clone.Protocol
}

// ToPB .
func (s *Server) ToPB() *grpc_server_list.Server {
	return &grpc_server_list.Server{
		Code:     s.Code,
		Name:     s.Name,
		Addr:     s.Addr,
		Protocol: s.Protocol,
	}
}

// Manipulator 數據庫操縱器
type Manipulator struct {
}

func (m Manipulator) tagGetByCode() string {
	return "module/server_list/Manipulator.GetByCode"
}

// GetByCode 通過 編碼 返回 服務器 信息
func (m Manipulator) GetByCode(code string) (info *Server, e error) {
	if code == "" {
		return
	}
	bean := &Server{
		Code: code,
	}
	ok, e := manipulator.Engine().Get(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagGetByCode()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	} else if ok {
		info = bean
	}
	return
}
func (m Manipulator) tagNew() string {
	return "module/server_list/Manipulator.New"
}

// New 增加 一個 服務器
func (m Manipulator) New(info *Server) (e error) {
	if info.Code == "" {
		e = errors.New("code not specified")
		return
	}
	_, e = manipulator.Engine().InsertOne(info)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagNew()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
func (m Manipulator) tagUpdate() string {
	return "module/server_list/Manipulator.Update"
}

// Update 更新 服務器 信息
func (m Manipulator) Update(info *Server) (rows int64, e error) {
	if info.Code == "" {
		e = errors.New("code not specified")
		return
	}
	rows, e = manipulator.Engine().Update(info, &Server{Code: info.Code})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
func (m Manipulator) tagRemove() string {
	return "module/server_list/Manipulator.Remove"
}

// Remove 刪除 一個 服務器
func (m Manipulator) Remove(code string) (rows int64, e error) {
	if code == "" {
		e = errors.New("code not specified")
		return
	}
	rows, e = manipulator.Engine().Delete(&Server{
		Code: code,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagRemove()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
func (m Manipulator) tagCount() string {
	return "module/server_list/Manipulator.Count"
}

// Count 統計 記錄 數
func (m Manipulator) Count(args *FindArgs) (rows int64, e error) {
	session := manipulator.Session()
	m.initFindSession(session, args)
	rows, e = session.Count(&Server{})
	session.Close()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagCount()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
func (m Manipulator) initFindSession(session *xorm.Session, args *FindArgs) {
	if args.mode == "" {
		return
	}
	if args.Code != "" {
		session.Where(
			fmt.Sprintf("%v %v ?", ColCode, args.mode),
			args.Code,
		)
	}
	if args.Name != "" {
		session.Where(
			fmt.Sprintf("%v %v ?", ColName, args.mode),
			args.Name,
		)
	}
	if args.Addr != "" {
		session.Where(
			fmt.Sprintf("%v %v ?", ColAddr, args.mode),
			args.Addr,
		)
	}

	if args.Protocol != "" {
		session.Where(
			fmt.Sprintf("%v %v ?", ColProtocol, args.mode),
			args.Protocol,
		)
	}
}
func (m Manipulator) tagFind() string {
	return "module/server_list/Manipulator.Find"
}

// Find 查詢 記錄
func (m Manipulator) Find(args *FindArgs, limit, start int) (beans []Server, e error) {
	session := manipulator.Session()
	m.initFindSession(session, args)
	if limit < 1 {
		e = session.Find(&beans)
	} else {
		e = session.Limit(limit, start).Find(&beans)
	}
	session.Close()
	if e != nil {
		beans = nil
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagFind()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
