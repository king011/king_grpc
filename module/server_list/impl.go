package list

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"king_grpc/logger"
	grpc_server_list "king_grpc/protocol/module/server_list"

	"go.uber.org/zap"
)

type _Impl struct {
}

func (s _Impl) tagGet() string {
	return "module/server_list Impl.Get"
}

func (s _Impl) Get(ctx context.Context, request *grpc_server_list.GetRequest) (response *grpc_server_list.GetResponse, e error) {
	var m Manipulator
	info, e := m.GetByCode(request.Code)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagGet()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	} else if info == nil {
		e = status.Errorf(codes.NotFound, "not found server %v", request.Code)
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagGet()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}

	response = &grpc_server_list.GetResponse{
		Data: &grpc_server_list.Server{
			Code:     info.Code,
			Name:     info.Name,
			Addr:     info.Addr,
			Protocol: info.Protocol,
		},
	}
	return
}
func (s _Impl) tagAdd() string {
	return "module/server_list Impl.Add"
}

var addResponse grpc_server_list.AddResponse

func (s _Impl) Add(ctx context.Context, request *grpc_server_list.AddRequest) (response *grpc_server_list.AddResponse, e error) {
	if request.Data == nil {
		e = status.Errorf(codes.InvalidArgument, "Data not support nil")
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagAdd()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	var m Manipulator
	var bean Server
	bean.FromPB(request.Data)
	e = m.New(&bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagAdd()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &addResponse
	return
}
func (s _Impl) tagUpdate() string {
	return "module/server_list Impl.Update"
}

func (s _Impl) Update(ctx context.Context, request *grpc_server_list.UpdateRequest) (response *grpc_server_list.UpdateResponse, e error) {
	if request.Data == nil {
		e = status.Errorf(codes.InvalidArgument, "Data not support nil")
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	var m Manipulator
	var bean Server
	bean.FromPB(request.Data)
	rows, e := m.Update(&bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_server_list.UpdateResponse{
		Rows: rows,
	}
	return
}
func (s _Impl) tagRemove() string {
	return "module/server_list Impl.Remove"
}

func (s _Impl) Remove(ctx context.Context, request *grpc_server_list.RemoveRequest) (response *grpc_server_list.RemoveResponse, e error) {
	var m Manipulator
	rows, e := m.Remove(request.Code)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagRemove()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_server_list.RemoveResponse{
		Rows: rows,
	}
	return
}
func (s _Impl) tagCount() string {
	return "module/server_list Impl.Count"
}

func (s _Impl) Count(ctx context.Context, request *grpc_server_list.CountRequest) (response *grpc_server_list.CountResponse, e error) {
	var args FindArgs
	e = args.FromPB(request.Args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCount()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	var m Manipulator
	rows, e := m.Count(&args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCount()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_server_list.CountResponse{
		Count: rows,
	}
	return
}
func (s _Impl) tagFind() string {
	return "module/server_list Impl.Find"
}

func (s _Impl) Find(ctx context.Context, request *grpc_server_list.FindRequest) (response *grpc_server_list.FindResponse, e error) {
	var args FindArgs
	e = args.FromPB(request.Args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	limit := int(request.Limit)
	start := int(request.Start)
	if limit < 1 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("limitSrc", limit),
				zap.Int("limitAdjust", 5),
			)
		}
		limit = 5
	} else if limit > 20 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("limitSrc", limit),
				zap.Int("limitAdjust", 20),
			)
		}
		limit = 20
	}
	if start < 0 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("startSrc", start),
				zap.Int("startAdjust", 0),
			)
		}
		start = 0
	}
	var m Manipulator
	beans, e := m.Find(&args, limit, start)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_server_list.FindResponse{}
	if len(beans) != 0 {
		response.Data = make([]*grpc_server_list.Server, len(beans))
		for i := 0; i < len(beans); i++ {
			response.Data[i] = beans[i].ToPB()
		}
	}
	return
}
