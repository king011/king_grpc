// Package list 爲手機程式 提供了 依據 服務器編碼 查詢服務器 信息的 服務
package list

import (
	"encoding/json"
	"king_grpc/db/manipulator"
	"king_grpc/module"
	grpc_server_list "king_grpc/protocol/module/server_list"

	"google.golang.org/grpc"
)

// ModuleID 子模塊 名稱
const ModuleID = "server_list"

func init() {
	module.Single().Register(&_Module{})
}

// _Module 實現模塊接口
type _Module struct {
	module.Base
}

// ID 返回 唯一的子模塊 名稱
func (m *_Module) ID() string {
	return ModuleID
}

// OnStart 初始化 回調 應該在此 執行 模塊 初始化
func (m *_Module) OnStart(basePath string, data json.RawMessage) {
	session, e := manipulator.Transaction()
	if e != nil {
		return
	}
	e = manipulator.InitDB(session, &Server{})
	manipulator.CloseTransaction(session, &e)
}

// RegisterGRPC 爲子模塊 註冊 grpc 服務
func (m *_Module) RegisterGRPC(srv *grpc.Server) {
	grpc_server_list.RegisterServiceServer(srv, _Impl{})
}
func (m *_Module) OnClearDBCache(tag string) error {
	return manipulator.ClearCache(TableName)
}

// Client 返回 客戶端 接口
//
// 如果返回 nil 則 不支持 客戶端
func (m *_Module) Client() module.OnClient {
	return m
}

// OnClient 執行 客戶端 操作
func (m *_Module) OnClient(client module.Client) bool {
	c := _Client{
		InputReader: client.IO(),
		client:      client,
	}
	return c.Run()
}
