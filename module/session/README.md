# 模塊信息

id : session

此模塊 爲客戶端 提供了 session 的 建立 保活 以及 返回 session 信息的 功能

# 路由和權限

insert router (name,val,rule) values 
    ("公共接口",0,"s:/module.session.service/certificate"), 

insert router (name,val,rule) values 
    ("session",2,"s:/module.session.service/"),

# 協議

pb/king_grpc/protocol/module/session

# 佔用數據庫

user

# 佔用 檔案/檔案夾

無