package session

import (
	"context"
	"crypto/sha512"
	"encoding/hex"
	"king_grpc/module"
	grpc_session "king_grpc/protocol/module/session"
	"king_grpc/utils"

	king_io "gitlab.com/king011/king-go/io"
)

type _Client struct {
	*king_io.InputReader
	client module.Client
	power  map[int32]string
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "cookie",
			Usage: "print cookie",
			Done: func() bool {
				c.Println("cookie :", c.client.GetCookie())
				return false
			},
		},

		utils.Command{
			Name:  "login",
			Usage: "request certificate",
			Done:  c.login,
		},
		utils.Command{
			Name:  "logout",
			Usage: "clear certificate",
			Done: func() bool {
				c.client.SetCookie("")
				c.Println("clear cookie")
				return false
			},
		},
		utils.Command{
			Name:  "keep",
			Usage: "request new certificate keep active",
			Done:  c.keep,
		},
		utils.Command{
			Name:  "info",
			Usage: "request session info",
			Done:  c.info,
		},
		utils.Command{
			Name:  "pwd",
			Usage: "change login password",
			Done:  c.pwd,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}
func (c *_Client) login() bool {
	name := c.ReadString("input user name")
	pwd := c.ReadString("input user password")
	data := sha512.Sum512([]byte(pwd))
	pwd = hex.EncodeToString(data[:])
	client := grpc_session.NewServiceClient(c.client.GetConn())
	response, e := client.Certificate(context.Background(),
		&grpc_session.CertificateRequest{
			Name:     name,
			Password: pwd,
		},
	)
	if e == nil {
		c.client.SetCookie(response.Val)
		c.Println("new cookie :", response.Val)
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) keep() bool {
	client := grpc_session.NewServiceClient(c.client.GetConn())
	response, e := client.Keep(c.client.Background(),
		&grpc_session.KeepRequest{},
	)
	if e == nil {
		c.client.SetCookie(response.Val)
		c.Println("keep cookie :", response.Val)
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) info() bool {
	client := grpc_session.NewServiceClient(c.client.GetConn())
	response, e := client.Info(c.client.Background(),
		&grpc_session.InfoRequest{},
	)
	if e == nil {
		c.Printf(`ID    = %v
Name  = %v
Power = %v
`,
			response.ID,
			response.Name,
			response.Power,
		)

	} else {
		c.Eprintln(e)
	}
	return false
}

func (c *_Client) pwd() bool {
	pwd := c.ReadString("input new password")
	data := sha512.Sum512([]byte(pwd))
	hash := hex.EncodeToString(data[:])

	client := grpc_session.NewServiceClient(c.client.GetConn())
	response, e := client.Password(c.client.Background(),
		&grpc_session.PasswordRequest{
			Password: hash,
		},
	)
	if e == nil {
		if response.Rows == 0 {
			c.Println("rows == 0")
		} else {
			c.Println("new password :", pwd)
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
