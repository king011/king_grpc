package session

import (
	"context"
	"king_grpc/cookie"
	"king_grpc/db/data"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"king_grpc/utils"
	"time"

	grpc_session "king_grpc/protocol/module/session"

	"go.uber.org/zap"
	"google.golang.org/grpc/metadata"
)

// Session 實現 grpc 服務
type Session struct {
}

func (s Session) tagCertificate() string {
	return "module/session Session.Certificate"
}

// Certificate 申請一個 用戶憑證
func (s Session) Certificate(ctx context.Context, request *grpc_session.CertificateRequest) (response *grpc_session.CertificateResponse, e error) {
	var mUser manipulator.User
	user, e := mUser.Login(request.Name, request.Password)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCertificate()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}

	val, e := user.ToSession().Cookie()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCertificate()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_session.CertificateResponse{
		Val: val,
	}
	return
}
func (s Session) tagKeep() string {
	return "module/session Session.Keep"
}

// Keep 刷新 用戶憑證 以免 過期
func (s Session) Keep(ctx context.Context, request *grpc_session.KeepRequest) (response *grpc_session.KeepResponse, e error) {
	var session *cookie.Session
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		session, e = cookie.FromMD(md)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagKeep()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			return
		}
	}
	if session == nil {
		e = utils.ErrSessionNil
		return
	}
	// session 超過 24 小時 刷新 權限
	if time.Now().After(session.Time.Add(time.Hour * 24)) {
		var mUser manipulator.User
		var user *data.User
		user, e = mUser.GetByID(session.ID)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagKeep()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			return
		}
		session.Time = time.Now()
		if user == nil {
			session.Power = nil
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagKeep()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.String("description", "keep not found user"),
					zap.Int64("id", session.ID),
					zap.String("name", session.Name),
				)
			}
		} else {
			session.Power = utils.SplitInt32(user.Power)
		}
	}
	val, e := session.Cookie()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagKeep()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_session.KeepResponse{
		Val: val,
	}
	return
}
func (s Session) tagInfo() string {
	return "module/session Session.Info"
}

// Info 返回 session 信息
func (s Session) Info(ctx context.Context, request *grpc_session.InfoRequest) (response *grpc_session.InfoResponse, e error) {
	var session *cookie.Session
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		session, e = cookie.FromMD(md)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagInfo()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			return
		}
	}
	if session == nil {
		e = utils.ErrSessionNil
		return
	}

	response = &grpc_session.InfoResponse{
		ID:    session.ID,
		Name:  session.Name,
		Power: session.Power,
	}
	return
}

func (s Session) tagPassword() string {
	return "module/session Session.Password"
}

// Password 修改登入密碼
func (s Session) Password(ctx context.Context, request *grpc_session.PasswordRequest) (response *grpc_session.PasswordResponse, e error) {
	var session *cookie.Session
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		session, e = cookie.FromMD(md)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagPassword()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			return
		}
	}
	if session == nil {
		e = utils.ErrSessionNil
		return
	}
	var mUser manipulator.User
	rows, e := mUser.Password(session.ID, request.Password)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagPassword()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}

	response = &grpc_session.PasswordResponse{
		Rows: rows,
	}
	return
}
