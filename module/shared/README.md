# 模塊信息

id : shared

此模塊 爲所有 登入用戶 提供了 共享的服務

# 路由和權限

insert router (name,val,rule) values 
    ("session",2,"s:/module.shared.service/"),

# 協議

pb/king_grpc/protocol/module/shared

# 佔用數據庫

router

# 佔用 檔案/檔案夾

無