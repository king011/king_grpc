package shared

import (
	"encoding/json"
	"king_grpc/module"
	grpc_shared "king_grpc/protocol/module/shared"
	"king_grpc/utils"

	king_io "gitlab.com/king011/king-go/io"
)

type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "router",
			Usage: "get router list",
			Done:  c.router,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}

func (c *_Client) router() bool {
	client := grpc_shared.NewServiceClient(c.client.GetConn())
	response, e := client.Router(c.client.Background(),
		&grpc_shared.RouterRequest{},
	)
	if e == nil {
		if len(response.Data) != 0 {
			b, e := json.MarshalIndent(response.Data, "", "\t")
			if e == nil {
				c.Println(string(b))
			} else {
				c.Eprintln(e)
			}
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
