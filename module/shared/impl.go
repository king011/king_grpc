package shared

import (
	"context"

	"king_grpc/db/data"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	grpc_shared "king_grpc/protocol/module/shared"

	"go.uber.org/zap"
)

type _Impl struct {
}

func (s _Impl) tagPing() string {
	return "module/shared Impl.Router"
}

func (s _Impl) Router(ctx context.Context, request *grpc_shared.RouterRequest) (response *grpc_shared.RouterResponse, e error) {
	var items []data.Router
	e = manipulator.Engine().Find(&items)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, s.tagPing()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_shared.RouterResponse{}
	if len(items) != 0 {
		response.Data = make([]*grpc_shared.Router, len(items))
		for i := 0; i < len(items); i++ {
			response.Data[i] = &grpc_shared.Router{
				ID:   items[i].ID,
				PID:  items[i].PID,
				Name: items[i].Name,
				Val:  items[i].Val,
			}
		}
	}
	return
}
