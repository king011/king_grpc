# 模塊信息

id : simple_file

此模塊 爲用戶 綁定了一個 檔案 供其上傳 備份數據

# 路由和權限

insert router (name,val,rule) values 
    ("session",200,"s:/module.simple_file.service/"),

# 協議

pb/king_grpc/protocol/module/simple_file

# 佔用數據庫

module_simple_file

# 佔用 檔案/檔案夾

/simple_file/*