package file

import (
	"context"
	"fmt"
	"io"
	"king_grpc/module"
	grpc_simple_file "king_grpc/protocol/module/simple_file"
	"king_grpc/utils"
	"os"
	"path/filepath"

	king_io "gitlab.com/king011/king-go/io"
)

// IFileReader 檔案 讀取接口
type IFileReader interface {
	io.ReadSeeker
	io.Closer
}
type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "upload",
			Usage: "upload file to server",
			Done: func() bool {
				if e := c.upload(); e != nil {
					c.Eprintln(e)
				}
				return false
			},
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}

func (c *_Client) upload() (e error) {
	filename := c.ReadString("input upload file path")
	var f IFileReader
	f, e = os.Open(filename)
	if e != nil {
		return
	}
	name := filepath.Base(filename)

	defer f.Close()
	h, e := utils.HashReader(f)
	if e != nil {
		return
	}
	f.Seek(0, os.SEEK_SET)

	client := grpc_simple_file.NewServiceClient(c.client.GetConn())
	ctx, cancel := context.WithCancel(c.client.Background())
	defer cancel()
	stream, e := client.Upload(ctx)
	if e != nil {
		return
	}
	// 發送 md5
	e = stream.Send(&grpc_simple_file.UploadRequest{
		Name: name,
		Hash: h,
	})
	if e != nil {
		return
	}
	var response grpc_simple_file.UploadResponse
	e = stream.RecvMsg(&response)
	if e != nil {
		return
	}
	if response.Val == grpc_simple_file.UploadResponse_Completed {
		// 已經 上傳過 同個 檔案 直接返回
		c.Println("cached")
		return
	} else if response.Val != grpc_simple_file.UploadResponse_Locked {
		e = fmt.Errorf("not support status [%v]", response.Val)
		return
	}
	// 驗證 緩存
	var offset int64
	if response.Offset != 0 {
		// 計算 hash
		lr := io.LimitReader(f, response.Offset)
		var hash string
		hash, e = utils.HashReader(lr)
		if e != nil {
			return
		}
		if hash == response.Hash {
			offset = response.Offset
		} else {
			f.Seek(0, os.SEEK_SET)
		}
	}
	if offset == 0 {
		c.Println("new upload")
	} else {
		c.Println("restore upload", offset)
	}
	b := make([]byte, 1024*16)
	var n int
	for {
		n, e = f.Read(b)
		if e != nil {
			if e == io.EOF {
				e = nil
				break
			}
			return
		}
		if n == 0 {
			continue
		}
		e = stream.Send(&grpc_simple_file.UploadRequest{
			Offset: offset,
			Data:   b[:n],
		})
		if e != nil {
			return
		}
		offset += int64(n)
	}
	e = stream.CloseSend()
	if e != nil {
		return
	}

	// 等待 完成 上傳
	e = stream.RecvMsg(&response)
	if e != nil {
		return
	}
	if response.Val == grpc_simple_file.UploadResponse_Completed {
		// 成功
	} else {
		e = fmt.Errorf("not support status [%v]", response.Val)
		return
	}
	return
}
