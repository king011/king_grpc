package file

import (
	"io/ioutil"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"king_grpc/utils"
	"os"
	"path/filepath"

	"go.uber.org/zap"
)

const (
	// ColID .
	ColID = "id"
	// ColName .
	ColName = "name"
	// ColDescription .
	ColDescription = "description"
	// ColHash .
	ColHash = "hash"
	// ColLock .
	ColLock = "lock"
	// ColTempFile .
	ColTempFile = "temp_file"
	// ColDstFile .
	ColDstFile = "dst_file"
	// TableName .
	TableName = "module_simple_file"
)

// Data 此模塊 爲每個 用戶 綁定了一個 檔案 供其上傳 下載 查看
type Data struct {
	// 關聯的 用戶 id
	ID int64 `xorm:"pk 'id'"`
	// 保存的 檔案名稱
	Name string `xorm:"notnull"`
	// 檔案 描述 信息
	Description string `xorm:"notnull"`

	// 緩存 上傳 成功 的檔案 hash 值
	Hash string `xorm:"notnull"`

	// 鎖定檔案
	// 用於 上傳時 防止 同個 session 同時進行多個上傳 請求
	Lock bool `xorm:"notnull"`

	// 檔案 磁盤中的 臨時 名稱
	TempFile string `xorm:"notnull"`
	// 檔案 磁盤中的 名稱 如果爲空 則 不存在
	DstFile string `xorm:"notnull"`
}

// TableName .
func (Data) TableName() string {
	return TableName
}

// Manipulator 數據庫操縱器
type Manipulator struct {
}

func (m Manipulator) tagLock() string {
	return "module/simple_file/Manipulator.Lock"
}

// Lock 鎖定檔案
//
// 如果 和 已存在檔案 一樣 返回 cached == true
//
// 否則 返回 cached == false 和 臨時檔案名
func (m Manipulator) Lock(id int64, hash string) (cached bool, lastFile, tempFile string, e error) {
	if id == 0 {
		e = utils.ErrIDNil
		return
	}

	// 啓動 事務
	session, e := manipulator.Transaction()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	defer manipulator.CloseTransaction(session, &e)

	// 查找 檔案
	bean := &Data{
		ID: id,
	}
	ok, e := session.Get(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	if ok {
		// 匹配 緩存 成功
		if bean.Hash == hash {
			cached = true
			return
		}
		if bean.TempFile == "" {
			var f *os.File
			f, e = ioutil.TempFile(_SimpleFile, "")
			if e != nil {
				if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Error(e),
					)
				}
				return
			}
			tempFile = f.Name()
		} else {
			tempFile = m.Path(bean.TempFile)
		}
		lastFile = bean.DstFile
		if bean.Lock {
			e = utils.ErrAlreadyLocked
			return
		}
		// 鎖定
		_, e = session.ID(id).Cols(
			ColLock,
			ColTempFile,
		).Update(&Data{
			Lock:     true,
			TempFile: filepath.Base(tempFile),
		})
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
				ce.Write(
					logger.GroupDB,
					zap.Error(e),
				)
			}
			return
		}
	} else {
		// 創建 臨時檔案
		var f *os.File
		f, e = ioutil.TempFile(_SimpleFile, "")
		if e != nil {
			if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			return
		}
		tempFile = f.Name()
		f.Close()
		// 鎖定
		_, e = session.InsertOne(&Data{
			ID:       id,
			Lock:     true,
			TempFile: filepath.Base(tempFile),
		})
		if e != nil {
			os.Remove(tempFile)
			if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
				ce.Write(
					logger.GroupDB,
					zap.Error(e),
				)
			}
			return
		}
	}
	return
}

// Path 返回 檔案 路徑
func (m Manipulator) Path(name string) string {
	return _SimpleFile + "/" + name
}

// MustHash 計算檔案 hash
func (m Manipulator) MustHash(filename string) (hash string, n int64, e error) {
	f, e := os.Open(filename)
	if e != nil {
		return
	}
	hash, n, e = utils.HashReader2(f)
	f.Close()
	return
}

// Commit 提交 成功
func (m Manipulator) Commit(id int64, last, dst, name, description, hash string) (e error) {
	// 更新 數據庫
	_, e = manipulator.Engine().ID(id).
		Cols(
			ColName,
			ColDescription,
			ColHash,
			ColTempFile,
			ColDstFile,
			ColLock,
		).Update(&Data{
		Name:        name,
		Description: description,
		Hash:        hash,
		TempFile:    "",
		DstFile:     filepath.Base(dst),
		Lock:        false,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	if last != "" {
		os.Remove(m.Path(last))
	}
	return
}

// Rollback 提交 失敗
func (m Manipulator) Rollback(id int64) (e error) {
	_, e = manipulator.Engine().ID(id).Cols(ColLock).Update(&Data{})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagLock()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	return
}

// unlockAll 提交 失敗
func (Manipulator) unlockAll() (e error) {
	_, e = manipulator.Engine().Cols(ColLock).Update(&Data{})
	return
}
