package file

import (
	"king_grpc/configure"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"os"
	"path/filepath"

	"gitlab.com/king011/king-go/os/fileperm"
	"go.uber.org/zap"
)

var _SimpleFile string

func initFilesystem() {
	cnf := configure.Single()
	// 創建 檔案夾
	_SimpleFile = cnf.Fileroot + "/simple_file"
	e := os.MkdirAll(_SimpleFile, fileperm.Directory)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "init simple_file"); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// 恢復 鎖定狀態
	var m Manipulator
	e = m.unlockAll()
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "unlock simple_file"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}

	// 查詢 檔案
	keys := make(map[string]bool)
	var beans []Data
	e = manipulator.Engine().Find(&beans)
	if e != nil {
		if ce := logger.Logger.Check(zap.FatalLevel, "unlock simple_file"); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		os.Exit(1)
	}
	for i := 0; i < len(beans); i++ {
		if beans[i].TempFile != "" {
			keys[beans[i].TempFile] = true
		}
		if beans[i].DstFile != "" {
			keys[beans[i].DstFile] = true
		}
	}

	// 移除 無效 檔案
	filepath.Walk(_SimpleFile, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			if _SimpleFile == path {
				return nil
			}
			return filepath.SkipDir
		}
		name := filepath.Base(path)
		if !keys[name] {
			err = os.Remove(path)
			if err != nil {
				if ce := logger.Logger.Check(zap.ErrorLevel, "remove simple_file"); ce != nil {
					ce.Write(
						logger.GroupSystem,
						zap.Error(e),
						zap.String("name", name),
					)
				}
			}
		}
		return nil
	})

}
