package file

import (
	"io"
	"king_grpc/cookie"
	"king_grpc/logger"
	grpc_simple_file "king_grpc/protocol/module/simple_file"
	"os"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type _Impl struct {
}

func (_Impl) tagUpload() string {
	return "module/simple_file Impl.Upload"
}

// Upload 上傳 檔案 到 服務器
// 1. 客戶端 傳輸 UploadRequest.Hash 到服務器
//    服務器 查詢檔案是否已經上傳 已經上傳 返回 Completed 完成 通信
//    如果 檔案未上傳 返回 Locked UploadResponse.Offset UploadResponse.Hash 後 等待客戶端 上傳後續 數據
//
// 2. 客戶端 依據 UploadResponse.Offset UploadResponse.Hash 判斷 是否 可以恢復斷點續傳
//    可以 設置 UploadRequest.Offset = UploadResponse.Offset  並且從此處 開始 上傳
//    不可以 設置 UploadRequest.Offset = 0 從0開始上傳
//
// 3. 服務器 查看 UploadRequest.Offset 爲0 創建新檔案 否則 添加到檔案尾
//    服務器 讀取到 io.EOF 代表 上傳 結束 完成 最後的 工作 並且 傳輸 Completed 通知 客戶端 上傳成功
//
// 4. 客戶端 傳輸玩數據後 需要 關閉 send 通道 服務器 得到 io.EOF 完成 檔案的最後工作 並返回 Completed
func (s _Impl) Upload(stream grpc_simple_file.Service_UploadServer) (e error) {
	// 獲取 session
	ctx := stream.Context()
	session, e := cookie.FromContext(ctx)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	} else if session == nil {
		e = status.Error(codes.PermissionDenied, "session nil")
		if ce := logger.Logger.Check(zap.ErrorLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}

	// 接收 初始化 請求
	var request grpc_simple_file.UploadRequest
	var response grpc_simple_file.UploadResponse
	e = stream.RecvMsg(&request)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		return
	}
	// 驗證 請求 參數
	name := request.Name
	description := request.Description
	hash := request.Hash
	if hash == "" {
		e = status.Error(codes.InvalidArgument, "hash can't empty")
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	logger.Logger.Debug("lock",
		zap.Int64("id", session.ID),
		zap.String("name", name),
		zap.String("hash", hash),
	)
	// 鎖定檔案 驗證 緩存
	var mSimpleFile Manipulator
	cached, lastFile, tmpFile, e := mSimpleFile.Lock(session.ID, request.Hash)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	} else if cached {
		logger.Logger.Debug("lock cached",
			zap.Int64("id", session.ID),
			zap.String("name", name),
			zap.String("hash", hash),
		)
		// 緩存 匹配 直接 返回 成功
		response.Val = grpc_simple_file.UploadResponse_Completed
		e = stream.Send(&response)
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		return
	}
	rollback := true
	defer func() {
		if rollback {
			mSimpleFile.Rollback(session.ID)
		}
	}()
	// 緩存 不匹配 返回 locked
	response.Val = grpc_simple_file.UploadResponse_Locked
	response.Hash, response.Offset, e = mSimpleFile.MustHash(tmpFile)
	logger.Logger.Debug("send lock",
		zap.Int64("id", session.ID),
		zap.Int64("offset", response.Offset),
		zap.String("hash", response.Hash),
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		return
	}
	e = stream.Send(&response)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		return
	}

	// 接收 檔案
	e = s.recvFile(stream, tmpFile)
	if e != nil {
		return
	}

	// 提交 完成 並解鎖
	e = mSimpleFile.Commit(session.ID, lastFile, tmpFile, name, description, hash)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	rollback = false

	// 通知 客戶端 成功
	response.Val = grpc_simple_file.UploadResponse_Completed
	e = stream.Send(&response)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
			ce.Write(
				logger.GroupGRPC,
				zap.Error(e),
			)
		}
		return
	}
	return
}

func (s _Impl) recvFile(stream grpc_simple_file.Service_UploadServer, dst string) (e error) {
	// 接收 檔案上傳 數據
	var request grpc_simple_file.UploadRequest
	var f *os.File
	for {
		e = stream.RecvMsg(&request)
		if e != nil {
			if e == io.EOF {
				e = nil
				break
			}
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
				ce.Write(
					logger.GroupGRPC,
					zap.Error(e),
				)
			}
			break
		}

		// 創建/打開 檔案
		if f == nil {
			if request.Offset == 0 {

				logger.Logger.Debug("create file",
					zap.String("dst", dst),
				)
				// 創建 新 檔案
				f, e = os.Create(dst)
				if e != nil {
					if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
						ce.Write(
							logger.GroupSystem,
							zap.Error(e),
						)
					}
					break
				}
			} else {
				logger.Logger.Debug("open file",
					zap.String("dst", dst),
				)
				f, e = os.OpenFile(dst, os.O_WRONLY|os.O_APPEND, 0)
				if e != nil {
					if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
						ce.Write(
							logger.GroupSystem,
							zap.Error(e),
						)
					}
					break
				}
			}
		}
		// 寫入數據
		_, e = f.Write(request.Data)
		if e != nil {
			if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpload()); ce != nil {
				ce.Write(
					logger.GroupSystem,
					zap.Error(e),
				)
			}
			break
		}
	}
	if f != nil {
		f.Close()
	}
	return
}
