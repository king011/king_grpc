// Package file 爲用戶綁定了一個 上傳檔案
package file

import (
	"encoding/json"
	"king_grpc/db/manipulator"
	"king_grpc/module"
	grpc_simple_file "king_grpc/protocol/module/simple_file"

	"google.golang.org/grpc"
)

// ModuleID 子模塊 名稱
const ModuleID = "simple_file"

func init() {
	module.Single().Register(&_Module{})
}

// _Module 實現模塊接口
type _Module struct {
	module.Base
}

// ID 返回 唯一的子模塊 名稱
func (m *_Module) ID() string {
	return ModuleID
}

// OnStart 初始化 回調 應該在此 執行 模塊 初始化
func (m *_Module) OnStart(basePath string, data json.RawMessage) {
	// 初始化 數據庫
	session, e := manipulator.Transaction()
	if e != nil {
		return
	}
	e = manipulator.InitDB(session, &Data{})
	manipulator.CloseTransaction(session, &e)

	// 初始化 檔案系統
	initFilesystem()
}

// RegisterGRPC 爲子模塊 註冊 grpc 服務
func (m *_Module) RegisterGRPC(srv *grpc.Server) {
	grpc_simple_file.RegisterServiceServer(srv, _Impl{})
}
func (m *_Module) OnClearDBCache(tag string) error {
	return manipulator.ClearCache(TableName)
}

// Client 返回 客戶端 接口
//
// 如果返回 nil 則 不支持 客戶端
func (m *_Module) Client() module.OnClient {
	return m
}

// OnClient 執行 客戶端 操作
func (m *_Module) OnClient(client module.Client) bool {
	c := _Client{
		InputReader: client.IO(),
		client:      client,
	}
	return c.Run()
}
