# 模塊信息

id : system

此模塊 爲管理員 提供了 服務器 運行管理功能

# 路由和權限

insert router (name,val,rule) values 
    ("system",4,"s:/module.system.service/"),

# 協議

pb/king_grpc/protocol/module/system

# 佔用數據庫

無

# 佔用 檔案/檔案夾

無