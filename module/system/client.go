package system

import (
	"bytes"
	"encoding/json"
	"fmt"
	"king_grpc/module"
	grpc_system "king_grpc/protocol/module/system"
	"king_grpc/utils"
	"time"

	king_io "gitlab.com/king011/king-go/io"
)

const (
	kb = 1024
	mb = 1024 * kb
	gb = 1024 * mb
)

type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "info",
			Usage: "get running info",
			Done:  c.info,
		},
		utils.Command{
			Name:  "module",
			Usage: "get all module id",
			Done:  c.module,
		},
		utils.Command{
			Name:  "router",
			Usage: "get server router rules",
			Done:  c.router,
		},
		utils.Command{
			Name:  "router r",
			Usage: "reload router",
			Done:  c.routerReload,
		},
		utils.Command{
			Name:  "reload",
			Usage: "reload configure",
			Done:  c.reload,
		},

		utils.Command{
			Name:  "clear",
			Usage: "clear cache",
			Done:  c.clear,
		},
		utils.Command{
			Name:  "clear db",
			Usage: "clear db cache",
			Done:  c.clearDB,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}
func (c *_Client) printJSON(v interface{}) {
	b, e := json.MarshalIndent(v, "", "\t")
	if e == nil {
		c.Println(string(b))
	} else {
		c.Eprintln(e)
	}
}
func (c *_Client) info() bool {
	client := grpc_system.NewServiceClient(c.client.GetConn())
	response, e := client.Info(c.client.Background(),
		&grpc_system.InfoRequest{},
	)
	if e == nil {
		started := time.Unix(response.Started, 0)
		c.Printf(`%v %v %v
Tag       = %v
Commit    = %v
Date      = %v
Started   = %v [%v]
PROCS     = %v
Goroutine = %v
Alloc     = %v [%v]
Sys       = %v [%v]
`,
			response.OS, response.ARCH, response.GO,
			response.Tag, response.Commit, response.Date,

			started, time.Now().Sub(started),
			response.PROCS,
			response.Goroutine,
			c.memFormat(response.Alloc), response.Alloc,
			c.memFormat(response.Sys), response.Sys,
		)

	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) memFormat(v uint64) string {
	var buf bytes.Buffer
	if v >= gb {
		buf.WriteString(fmt.Sprintf("%vg", v/gb))
		v %= gb
	}
	if v >= mb {
		buf.WriteString(fmt.Sprintf("%vm", v/mb))
		v %= mb
	}
	if v >= kb {
		buf.WriteString(fmt.Sprintf("%vk", v/kb))
		v %= kb
	}
	if v > 0 {
		buf.WriteString(fmt.Sprint(v))
	}

	return buf.String()
}

func (c *_Client) routerReload() bool {
	client := grpc_system.NewServiceClient(c.client.GetConn())
	_, e := client.RouterReload(c.client.Background(),
		&grpc_system.RouterReloadRequest{},
	)
	if e == nil {
		c.Println("router reload succes")
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) reload() bool {
	client := grpc_system.NewServiceClient(c.client.GetConn())
	_, e := client.Reload(c.client.Background(),
		&grpc_system.ReloadRequest{},
	)
	if e == nil {
		c.Println("reload succes")
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) module() bool {
	client := grpc_system.NewServiceClient(c.client.GetConn())
	response, e := client.Module(c.client.Background(),
		&grpc_system.ModuleRequest{},
	)
	if e == nil {
		c.Println("module :")
		for i := 0; i < len(response.Data); i++ {
			fmt.Println("   ", response.Data[i])
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) router() bool {
	client := grpc_system.NewServiceClient(c.client.GetConn())
	response, e := client.Router(c.client.Background(),
		&grpc_system.RouterRequest{},
	)
	if e == nil {
		c.printJSON(response.Data)
	} else {
		c.Eprintln(e)
	}
	return false
}

func (c *_Client) clear() bool {
	m := c.ReadString("input module or s<sure> or c<cancel>")
	if m == "c" {
		return false
	} else if m == "s" {
		m = ""
	}
	tag := c.ReadString("tag module or s<sure> or c<cancel>")
	if tag == "c" {
		return false
	} else if tag == "s" {
		tag = ""
	}

	client := grpc_system.NewServiceClient(c.client.GetConn())
	_, e := client.ClearCache(c.client.Background(),
		&grpc_system.ClearCacheRequest{
			Module: m,
			Tag:    tag,
		},
	)
	if e == nil {
		c.Println("clear cache success")
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) clearDB() bool {
	m := c.ReadString("input module or s<sure> or c<cancel>")
	if m == "c" {
		return false
	} else if m == "s" {
		m = ""
	}
	tag := c.ReadString("tag module or s<sure> or c<cancel>")
	if tag == "c" {
		return false
	} else if tag == "s" {
		tag = ""
	}

	client := grpc_system.NewServiceClient(c.client.GetConn())
	_, e := client.ClearDBCache(c.client.Background(),
		&grpc_system.ClearDBCacheRequest{
			Module: m,
			Tag:    tag,
		},
	)
	if e == nil {
		c.Println("clear db cache success")
	} else {
		c.Eprintln(e)
	}
	return false
}
