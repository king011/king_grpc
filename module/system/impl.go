package system

import (
	"context"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"king_grpc/module"
	grpc_system "king_grpc/protocol/module/system"
	"king_grpc/version"
	"runtime"

	"go.uber.org/zap"
)

type _Impl struct {
}

func (s _Impl) tagInfo() string {
	return "module/system Impl.Info"
}

func (s _Impl) Info(ctx context.Context, request *grpc_system.InfoRequest) (response *grpc_system.InfoResponse, e error) {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	response = &grpc_system.InfoResponse{
		OS:   runtime.GOOS,
		ARCH: runtime.GOARCH,
		GO:   runtime.Version(),

		Tag:    version.Tag,
		Commit: version.Commit,
		Date:   version.Date,

		Started:   _Started.Unix(),
		PROCS:     int32(runtime.GOMAXPROCS(0)),
		Goroutine: int64(runtime.NumGoroutine()),
		Alloc:     m.Alloc,
		Sys:       m.Sys,
	}
	return
}
func (s _Impl) tagRouterReload() string {
	return "module/system Impl.RouterReload"
}

func (s _Impl) Router(ctx context.Context, request *grpc_system.RouterRequest) (response *grpc_system.RouterResponse, e error) {
	items := manipulator.RouterItems()
	response = &grpc_system.RouterResponse{}
	if len(items) != 0 {
		response.Data = make([]*grpc_system.Router, len(items))
		for i := 0; i < len(items); i++ {
			response.Data[i] = &grpc_system.Router{
				ID:   items[i].ID,
				PID:  items[i].PID,
				Name: items[i].Name,
				Val:  items[i].Val,
				Rule: items[i].Rule,
			}
		}
	}
	return
}

var routerReloadResponse grpc_system.RouterReloadResponse

func (s _Impl) RouterReload(ctx context.Context, request *grpc_system.RouterReloadRequest) (response *grpc_system.RouterReloadResponse, e error) {
	e = manipulator.RouterReload()
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagRouterReload()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &routerReloadResponse
	return
}
func (s _Impl) Module(ctx context.Context, request *grpc_system.ModuleRequest) (response *grpc_system.ModuleResponse, e error) {
	response = &grpc_system.ModuleResponse{
		Data: module.Single().ID(),
	}
	return
}
func (s _Impl) tagClearDBCache() string {
	return "module/system Impl.ClearDBCache"
}

var clearDBCacheResponse grpc_system.ClearDBCacheResponse

func (s _Impl) ClearDBCache(ctx context.Context, request *grpc_system.ClearDBCacheRequest) (response *grpc_system.ClearDBCacheResponse, e error) {
	e = module.Single().ClearDBCache(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagClearDBCache()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &clearDBCacheResponse
	return
}
func (s _Impl) tagClearCache() string {
	return "module/system Impl.ClearCache"
}

var clearCacheResponse grpc_system.ClearCacheResponse

func (s _Impl) ClearCache(ctx context.Context, request *grpc_system.ClearCacheRequest) (response *grpc_system.ClearCacheResponse, e error) {
	e = module.Single().ClearCache(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagClearCache()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &clearCacheResponse
	return
}
func (s _Impl) tagReload() string {
	return "module/system Impl.Reload"
}

var reloadResponse grpc_system.ReloadResponse

func (s _Impl) Reload(ctx context.Context, request *grpc_system.ReloadRequest) (response *grpc_system.ReloadResponse, e error) {
	e = module.Single().Reload(request.Module, request.Tag)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagClearCache()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &reloadResponse
	return
}
