// Package system 爲管理員 提供了 系統運行管理功能
package system

import (
	"encoding/json"
	"king_grpc/db/manipulator"
	"king_grpc/module"
	grpc_system "king_grpc/protocol/module/system"
	"time"

	"google.golang.org/grpc"
)

// ModuleID 子模塊 名稱
const ModuleID = "system"

var _Started time.Time

func init() {
	module.Single().Register(&_Module{})
}

// _Module 實現模塊接口
type _Module struct {
	module.Base
}

// ID 返回 唯一的子模塊 名稱
func (m *_Module) ID() string {
	return ModuleID
}

// OnStart 初始化 回調 應該在此 執行 模塊 初始化
func (m *_Module) OnStart(basePath string, data json.RawMessage) {
	_Started = time.Now()

	// fmt.Println("OnStart system")
	// fmt.Println("basePath ", basePath)
	// fmt.Println("data ", string(data))
}

// OnReload .
// func (m *_Module) OnReload(basePath string, data json.RawMessage, tag string) (e error) {
// 	fmt.Println("OnReload system")
// 	fmt.Println("basePath ", basePath)
// 	fmt.Println("data ", string(data))
// 	return
// }

// RegisterGRPC 爲子模塊 註冊 grpc 服務
func (m *_Module) RegisterGRPC(srv *grpc.Server) {
	grpc_system.RegisterServiceServer(srv, _Impl{})
}

// Client 返回 客戶端 接口
//
// 如果返回 nil 則 不支持 客戶端
func (m *_Module) Client() module.OnClient {
	return m
}

// OnClient 執行 客戶端 操作
func (m *_Module) OnClient(client module.Client) bool {
	c := _Client{
		InputReader: client.IO(),
		client:      client,
	}
	return c.Run()
}
func (m *_Module) OnClearDBCache(tag string) (e error) {
	engine := manipulator.Engine()
	if tag == "user" {
		e = engine.ClearCache(tag)
	} else if tag == "router" {
		e = engine.ClearCache(tag)
	} else {
		e = engine.ClearCache("user", "router")
	}
	return
}
