# 模塊信息

id : user

此模塊 爲管理員 提供了 用戶管理功能

# 路由和權限

insert router (name,val,rule) values 
    ("user",3,"s:/module.user.service/"),

# 協議

pb/king_grpc/protocol/module/user

# 佔用數據庫

user

# 佔用 檔案/檔案夾

無