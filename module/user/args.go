package user

import (
	grpc_user "king_grpc/protocol/module/user"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// FindArgs 查詢參數 定義
type FindArgs struct {
	// 查詢模式
	Mode grpc_user.FindMode
	// 查詢 條件 如果爲空 不限定 條件
	ID     int64
	Name   string
	Power  []int32
	Status uint32

	mode string
}

// FromPB .
func (args *FindArgs) FromPB(clone *grpc_user.FindArgs) (e error) {
	if clone == nil {
		args.Reset()
		return
	}

	if clone.Name != "" {
		switch clone.Mode {
		case grpc_user.FindMode_Equal:
			args.mode = "="
		case grpc_user.FindMode_Like:
			args.mode = "like"
		case grpc_user.FindMode_Regexp:
			args.mode = "regexp"
		default:
			e = status.Errorf(codes.InvalidArgument, "not support find mode : %v", clone.Mode)
			return
		}
	}

	args.Name = clone.Name
	args.ID = clone.ID
	args.Status = clone.Status
	if len(clone.Power) == 0 {
		args.Power = nil
	} else {
		powers := make([]int32, 0, len(clone.Power))
		keys := make(map[int32]bool)
		for _, power := range clone.Power {
			if power == 0 || keys[power] {
				continue
			}
			keys[power] = true
			powers = append(powers, power)
		}

		if len(powers) != 0 {
			args.Power = powers
		}
	}
	return
}

// Reset .
func (args *FindArgs) Reset() {
	if args.ID != 0 {
		args.ID = 0
	}
	if args.Name != "" {
		args.Name = ""
	}
	if args.Power != nil {
		args.Power = nil
	}
	if args.Status != 0 {
		args.Status = 0
	}
}
