package user

import (
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"king_grpc/db/data"
	"king_grpc/module"
	grpc_user "king_grpc/protocol/module/user"
	"king_grpc/utils"
	"strconv"

	king_io "gitlab.com/king011/king-go/io"
)

type _Client struct {
	*king_io.InputReader
	client module.Client
}

func (c *_Client) Run() bool {
	var management utils.Management
	run := true
	management.AddCommand(
		utils.Command{
			Name:  "h",
			Usage: "display help",
			Done: func() bool {
				c.Print(management.Usage())
				return false
			},
		},
		utils.Command{
			Name:  "q",
			Usage: "quit",
			Done: func() bool {
				return true
			},
		},
		utils.Command{
			Name:  "b",
			Usage: "backup",
			Done: func() bool {
				run = false
				return false
			},
		},

		utils.Command{
			Name:  "add",
			Usage: "add a new user",
			Done:  c.add,
		},
		utils.Command{
			Name:  "update",
			Usage: "update user info",
			Done:  c.update,
		},
		utils.Command{
			Name:  "remove",
			Usage: "remove user",
			Done:  c.remove,
		},
		utils.Command{
			Name:  "count",
			Usage: "count user",
			Done:  c.count,
		},
		utils.Command{
			Name:  "find",
			Usage: "find user",
			Done:  c.find,
		},
	)
	c.Print(management.Usage())

	var cmd string
	var exit bool
	for run {
		cmd = c.ReadString("cmd")
		exit, _ = management.Done(cmd)
		if exit {
			return true
		}
	}
	return false
}
func (c *_Client) printJSON(v interface{}) {
	b, e := json.MarshalIndent(v, "", "\t")
	if e == nil {
		c.Println(string(b))
	} else {
		c.Eprintln(e)
	}
}
func (c *_Client) add() bool {
	data := c.inputNew()
	if data == nil {
		return false
	}
	pwd := sha512.Sum512([]byte(data.Password))
	hash := hex.EncodeToString(pwd[:])
	client := grpc_user.NewServiceClient(c.client.GetConn())
	response, e := client.Add(c.client.Background(),
		&grpc_user.AddRequest{
			Name:     data.Name,
			Password: hash,
			Power:    data.Power,
			Status:   data.Status,
		},
	)
	if e == nil {
		c.Println("new success :", response.ID)
	} else {
		c.Eprintln(e)
	}
	return false
}

func (c *_Client) inputNew() (rs *grpc_user.AddRequest) {
	data := &grpc_user.AddRequest{}
	var cancel bool
	for {
		data.Name = c.ReadString("input name or c<cancel>")
		if data.Name == "c" {
			return
		}
		data.Password = c.ReadString("input password or c<cancel>")
		if data.Password == "c" {
			return
		}
		data.Power, cancel = c.inputPower()
		if cancel {
			return
		}
		data.Status, cancel = c.inputStatus(false)
		if cancel {
			return
		}
		for {
			c.printJSON(data)
			cmd := c.ReadString("s<sure> or c<cancal> or r<reset>")
			if cmd == "s" {
				rs = data
				return
			} else if cmd == "c" {
				return
			} else if cmd == "r" {
				break
			}
		}
	}
}
func (c *_Client) inputStatus(allowEmpty bool) (status uint32, cancel bool) {
	var v string
	for {
		if allowEmpty {
			v = c.ReadString(fmt.Sprintf("input status 0<any> %v<nil> %v<active> %v<disable> or c<cancal>",
				data.UserStatusNil,
				data.UserStatusActive,
				data.UserStatusDisable,
			))
		} else {
			v = c.ReadString(fmt.Sprintf("input status %v<nil> %v<active> %v<disable> or c<cancal>",
				data.UserStatusNil,
				data.UserStatusActive,
				data.UserStatusDisable,
			))
		}

		if v == "c" {
			cancel = true
			break
		} else if v == fmt.Sprint(data.UserStatusNil) {
			status = data.UserStatusNil
			break
		} else if v == fmt.Sprint(data.UserStatusActive) {
			status = data.UserStatusActive
			break
		} else if v == fmt.Sprint(data.UserStatusDisable) {
			status = data.UserStatusDisable
			break
		} else if v == "0" && allowEmpty {
			status = 0
			break
		}
	}
	return
}
func (c *_Client) inputPower() (arrs []int32, cancel bool) {
	keys := make(map[int32]bool)
	for {
		v := c.ReadString("input power or s<sure> or c<cancal> or r<reset> or d<display>")
		if v == "s" {
			if arrs != nil && len(arrs) == 0 {
				arrs = nil
			}
			break
		} else if v == "c" {
			cancel = true
			break
		} else if v == "d" {
			c.Println(arrs)
			for k := range keys {
				keys[k] = false
			}
			continue
		} else if v == "r" {
			c.Println("reset power")
			arrs = arrs[:0]
			continue
		}
		val, e := strconv.ParseInt(v, 10, 32)
		if e != nil {
			continue
		}
		power := int32(val)
		if keys[power] {
			continue
		}
		keys[power] = true
		arrs = append(arrs, power)
	}
	return
}
func (c *_Client) inputUpdate() (rs *grpc_user.UpdateRequest) {
	data := &grpc_user.UpdateRequest{}
	var cancel bool
	for {
		for data.ID == 0 {
			str := c.ReadString("input id or c<cancel>")
			if str == "c" {
				return
			}
			data.ID, _ = strconv.ParseInt(str, 10, 64)
		}
		data.Name = c.ReadString("input name or c<cancel>")
		if data.Name == "c" {
			return
		}
		data.Password = c.ReadString("input password or c<cancel>")
		if data.Password == "c" {
			return
		}
		data.Power, cancel = c.inputPower()
		if cancel {
			return
		}
		data.Status, cancel = c.inputStatus(false)
		if cancel {
			return
		}
		for {
			c.printJSON(data)
			cmd := c.ReadString("s<sure> or c<cancal> or r<reset>")
			if cmd == "s" {
				rs = data
				return
			} else if cmd == "c" {
				return
			} else if cmd == "r" {
				break
			}
		}
	}
}
func (c *_Client) update() bool {
	data := c.inputUpdate()
	if data == nil {
		return false
	}

	pwd := sha512.Sum512([]byte(data.Password))
	hash := hex.EncodeToString(pwd[:])
	client := grpc_user.NewServiceClient(c.client.GetConn())
	response, e := client.Update(c.client.Background(),
		&grpc_user.UpdateRequest{
			ID:       data.ID,
			Name:     data.Name,
			Password: hash,
			Power:    data.Power,
			Status:   data.Status,
		},
	)
	if e == nil {
		if response.Rows == 0 {
			c.Eprintln("not update user :", data.ID)
		} else {
			c.Println("update success")
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) remove() bool {
	var id int64
	for id == 0 {
		str := c.ReadString("input id or c<cancel>")
		if str == "c" {
			return false
		}
		id, _ = strconv.ParseInt(str, 10, 64)
	}

	client := grpc_user.NewServiceClient(c.client.GetConn())
	response, e := client.Remove(c.client.Background(),
		&grpc_user.RemoveRequest{
			ID: id,
		},
	)
	if e == nil {
		if response.Rows == 0 {
			c.Eprintln("not found user :", id)
		} else {
			c.Println("remove success")
		}
	} else {
		c.Eprintln(e)
	}
	return false
}
func (c *_Client) inputFind() (rs *grpc_user.FindArgs) {
	data := &grpc_user.FindArgs{}
	var cancel bool
	for {
		for {
			mode := c.ReadString("input mode =<equal> *<like> r<regexp> or c<cancel>")
			if mode == "c" {
				return
			} else if mode == "=" {
				data.Mode = grpc_user.FindMode_Equal
				break
			} else if mode == "*" {
				data.Mode = grpc_user.FindMode_Like
				break
			} else if mode == "r" {
				data.Mode = grpc_user.FindMode_Regexp
				break
			}
		}

		for data.ID != 0 {
			str := c.ReadString("input id or s<sure> or c<cancel>")
			if str == "c" {
				return
			} else if str == "s" {
				break
			}
			data.ID, _ = strconv.ParseInt(str, 10, 64)
		}
		data.Name = c.ReadString("input name or s<sure> or c<cancel>")
		if data.Name == "c" {
			return
		} else if data.Name == "s" {
			data.Name = ""
		}
		data.Power, cancel = c.inputPower()
		if cancel {
			return
		}
		data.Status, cancel = c.inputStatus(true)
		if cancel {
			return
		}
		for {
			c.printJSON(data)
			cmd := c.ReadString("s<sure> or c<cancal> or r<reset>")
			if cmd == "s" {
				rs = data
				return
			} else if cmd == "c" {
				return
			} else if cmd == "r" {
				break
			}
		}
	}
}
func (c *_Client) count() bool {
	args := c.inputFind()
	if args == nil {
		return false
	}

	client := grpc_user.NewServiceClient(c.client.GetConn())
	response, e := client.Count(c.client.Background(),
		&grpc_user.CountRequest{
			Args: args,
		},
	)
	if e == nil {
		c.Println("count =", response.Count)
	} else {
		c.Eprintln(e)
	}
	return false
}

func (c *_Client) find() bool {
	args := c.inputFind()
	if args == nil {
		return false
	}
	var limit int64 = -1
	for limit < 0 {
		limit = c.ReadNumber("input limit")
	}
	var start int64 = -1
	for start < 0 {
		start = c.ReadNumber("input start")
	}

	client := grpc_user.NewServiceClient(c.client.GetConn())
	response, e := client.Find(c.client.Background(),
		&grpc_user.FindRequest{
			Args:  args,
			Limit: limit,
			Start: start,
		},
	)
	if e == nil {
		c.printJSON(response.Data)
	} else {
		c.Eprintln(e)
	}
	return false
}
