package user

import (
	"fmt"
	"king_grpc/db/manipulator"
	"king_grpc/logger"
	"king_grpc/utils"
	"strings"

	grpc_user "king_grpc/protocol/module/user"

	"github.com/go-xorm/xorm"
	"go.uber.org/zap"
)

const (
	// ColID .
	ColID = "id"
	// ColName .
	ColName = "name"
	// ColPower .
	ColPower = "power"
	// ColPassword .
	ColPassword = "password"
	// ColStatus .
	ColStatus = "status"
)

// Data 數據庫 內存映射
type Data struct {
	// 唯一 id
	ID int64 `xorm:"pk autoincr 'id'"`

	// 用戶權限 以 ` 分隔多個權限
	Power string `xorm:"TEXT notnull"`

	// 用戶登入名稱
	Name string `xorm:"unique notnull"`

	// 用戶登入密碼 sha512
	Password string `xorm:"notnull"`

	// 用戶 狀態
	Status uint32 `xorm:"notnull"`
}

// TableName .
func (Data) TableName() string {
	return "user"
}

// ToPB .
func (d *Data) ToPB() *grpc_user.Data {
	return &grpc_user.Data{
		ID:     d.ID,
		Name:   d.Name,
		Power:  utils.SplitInt32(d.Power),
		Status: d.Status,
	}
}

// Manipulator 數據操作器
type Manipulator struct {
}

func (m Manipulator) tagNew() string {
	return "module/user/Manipulator.New"
}

// New 新建用戶
func (m Manipulator) New(name, password, power string, status uint32) (id int64, e error) {
	bean := &Data{
		Name:     name,
		Power:    power,
		Password: password,
		Status:   status,
	}
	_, e = manipulator.Engine().InsertOne(bean)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagNew()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	id = bean.ID
	return
}
func (m Manipulator) tagUpdate() string {
	return "module/user/Manipulator.Update"
}

// Update 更新 用戶 信息
func (m Manipulator) Update(id int64, name, password, power string, status uint32) (rows int64, e error) {
	rows, e = manipulator.Engine().ID(id).Update(&Data{
		Name:     name,
		Power:    power,
		Password: password,
		Status:   status,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	return
}
func (m Manipulator) tagRemove() string {
	return "module/user/Manipulator.Remove"
}

// Remove 刪除 一個 用戶
func (m Manipulator) Remove(id int64) (rows int64, e error) {
	if id == 0 {
		return
	}
	rows, e = manipulator.Engine().Delete(&Data{
		ID: id,
	})
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagRemove()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
		return
	}
	return
}
func (m Manipulator) tagCount() string {
	return "module/user/Manipulator.Count"
}

// Count 統計 記錄 數
func (m Manipulator) Count(args *FindArgs) (rows int64, e error) {
	session := manipulator.Session()
	m.initFindSession(session, args)
	rows, e = session.Count(&Data{})
	session.Close()
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagCount()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
func (m Manipulator) initFindSession(session *xorm.Session, args *FindArgs) {
	if args.ID != 0 {
		session.ID(args.ID)
	}

	// 如果是 正則配額 將 優先級 移到後面
	if args.mode != "" && args.Name != "" && args.Mode != grpc_user.FindMode_Regexp {
		session.Where(
			fmt.Sprintf("%v %v ?", ColName, args.mode),
			args.Name,
		)
	}
	if args.Status != 0 {
		session.Where(
			ColStatus+" = ?",
			args.Status,
		)
	}

	// 將正則匹配 放到 後面
	if args.mode != "" && args.Name != "" && args.Mode == grpc_user.FindMode_Regexp {
		session.Where(
			fmt.Sprintf("%v %v ?", ColName, args.mode),
			args.Name,
		)
	}
	if len(args.Power) > 0 {
		var match string
		if len(args.Power) == 1 {
			match = fmt.Sprintf("^(`?(-?\\d)*`)*%v(`(-?\\d)*`?)*$", args.Power[0])
		} else {
			strs := make([]string, len(args.Power))
			for i := 0; i < len(args.Power); i++ {
				strs[i] = fmt.Sprintf("(%v)", args.Power[i])
				//	strs[i] = fmt.Sprintf("(^(`?(-?\\d)*`)*%v(`(-?\\d)*`?)*$)", args.Power[i])
			}
			match = fmt.Sprintf("^(`?(-?\\d)*`)*(%v)(`(-?\\d)*`?)*$", strings.Join(strs, "|"))
		}
		session.Where(fmt.Sprintf(`%v regexp ?`, ColPower), match)
	}
	return
}
func (m Manipulator) tagFind() string {
	return "module/user/Manipulator.Find"
}

// Find 查詢 記錄
func (m Manipulator) Find(args *FindArgs, limit, start int) (beans []Data, e error) {
	session := manipulator.Session()
	m.initFindSession(session, args)
	if limit < 1 {
		e = session.Find(&beans)
	} else {
		e = session.Limit(limit, start).Find(&beans)
	}
	session.Close()
	if e != nil {
		beans = nil
		if ce := logger.Logger.Check(zap.ErrorLevel, m.tagFind()); ce != nil {
			ce.Write(
				logger.GroupDB,
				zap.Error(e),
			)
		}
	}
	return
}
