package user

import (
	"context"
	"king_grpc/logger"
	"king_grpc/utils"

	"google.golang.org/grpc/codes"

	"google.golang.org/grpc/status"

	"go.uber.org/zap"

	grpc_user "king_grpc/protocol/module/user"
)

type _Impl struct {
}

func (s _Impl) tagAdd() string {
	return "module/user Impl.Add"
}

func (s _Impl) Add(ctx context.Context, request *grpc_user.AddRequest) (response *grpc_user.AddResponse, e error) {
	power, e := utils.JoinInt32(request.Power)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, s.tagAdd()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}

	var m Manipulator
	id, e := m.New(request.Name,
		request.Password,
		power,
		request.Status,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagAdd()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_user.AddResponse{
		ID: id,
	}
	return
}
func (s _Impl) tagUpdate() string {
	return "module/user Impl.Update"
}
func (s _Impl) Update(ctx context.Context, request *grpc_user.UpdateRequest) (response *grpc_user.UpdateResponse, e error) {
	if request.ID == 0 {
		e = status.Error(codes.InvalidArgument, "id not specified")
		return
	}
	power, e := utils.JoinInt32(request.Power)
	if e != nil {
		if ce := logger.Logger.Check(zap.ErrorLevel, s.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}

	var m Manipulator
	rows, e := m.Update(
		request.ID,
		request.Name,
		request.Password,
		power,
		request.Status,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagUpdate()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_user.UpdateResponse{
		Rows: rows,
	}
	return
}
func (s _Impl) tagRemove() string {
	return "module/user Impl.Remove"
}
func (s _Impl) Remove(ctx context.Context, request *grpc_user.RemoveRequest) (response *grpc_user.RemoveResponse, e error) {
	if request.ID == 0 {
		e = status.Error(codes.InvalidArgument, "id not specified")
		return
	}

	var m Manipulator
	rows, e := m.Remove(
		request.ID,
	)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagRemove()); ce != nil {
			ce.Write(
				logger.GroupSystem,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_user.RemoveResponse{
		Rows: rows,
	}
	return
}
func (s _Impl) tagCount() string {
	return "module/user Impl.Count"
}

func (s _Impl) Count(ctx context.Context, request *grpc_user.CountRequest) (response *grpc_user.CountResponse, e error) {
	var args FindArgs
	e = args.FromPB(request.Args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCount()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	var m Manipulator
	rows, e := m.Count(&args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagCount()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_user.CountResponse{
		Count: rows,
	}
	return
}

func (s _Impl) tagFind() string {
	return "module/user Impl.Find"
}

func (s _Impl) Find(ctx context.Context, request *grpc_user.FindRequest) (response *grpc_user.FindResponse, e error) {
	var args FindArgs
	e = args.FromPB(request.Args)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	limit := int(request.Limit)
	start := int(request.Start)
	if limit < 1 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("limitSrc", limit),
				zap.Int("limitAdjust", 5),
			)
		}
		limit = 5
	} else if limit > 20 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("limitSrc", limit),
				zap.Int("limitAdjust", 20),
			)
		}
		limit = 20
	}
	if start < 0 {
		if ce := logger.Logger.Check(zap.DebugLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Int("startSrc", start),
				zap.Int("startAdjust", 0),
			)
		}
		start = 0
	}
	var m Manipulator
	beans, e := m.Find(&args, limit, start)
	if e != nil {
		if ce := logger.Logger.Check(zap.WarnLevel, s.tagFind()); ce != nil {
			ce.Write(
				logger.GroupAction,
				zap.Error(e),
			)
		}
		return
	}
	response = &grpc_user.FindResponse{}
	if len(beans) != 0 {
		response.Data = make([]*grpc_user.Data, len(beans))
		for i := 0; i < len(beans); i++ {
			response.Data[i] = beans[i].ToPB()
		}
	}
	return
}
