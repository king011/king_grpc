// Package user 爲管理員 提供了 用戶管理功能
package user

import (
	"king_grpc/module"
	grpc_user "king_grpc/protocol/module/user"

	"google.golang.org/grpc"
)

// ModuleID 子模塊 名稱
const ModuleID = "user"

func init() {
	module.Single().Register(&_Module{})
}

// _Module 實現模塊接口
type _Module struct {
	module.Base
}

// ID 返回 唯一的子模塊 名稱
func (m *_Module) ID() string {
	return ModuleID
}

// RegisterGRPC 爲子模塊 註冊 grpc 服務
func (m *_Module) RegisterGRPC(srv *grpc.Server) {
	grpc_user.RegisterServiceServer(srv, _Impl{})
}

// Client 返回 客戶端 接口
//
// 如果返回 nil 則 不支持 客戶端
func (m *_Module) Client() module.OnClient {
	return m
}

// OnClient 執行 客戶端 操作
func (m *_Module) OnClient(client module.Client) bool {
	c := _Client{
		InputReader: client.IO(),
		client:      client,
	}
	return c.Run()
}
