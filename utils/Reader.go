package utils

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// Reader 獲取 用戶 輸入
type Reader struct {
	rd *bufio.Reader
}

// NewReader 創建 用戶 輸入 reader
func NewReader(rd io.Reader) Reader {
	return Reader{
		rd: bufio.NewReader(rd),
	}
}

// ReadString 等待用戶 輸入字符串
func (r Reader) ReadString(placeholder string) (val string) {
	for {
		fmt.Printf("%s $ ", placeholder)
		b, _, e := r.rd.ReadLine()
		if e != nil {
			fmt.Println(e)
			continue
		}
		val = strings.TrimSpace(string(b))
		if val != "" {
			break
		}
	}
	return
}

// ReadStringDefault 等待用戶 輸入字符串
func (r Reader) ReadStringDefault(placeholder, def string) (val string) {
	for {
		fmt.Printf("%s [%v] $ ", placeholder, def)
		b, _, e := r.rd.ReadLine()
		if e != nil {
			fmt.Println(e)
			continue
		}
		val = strings.TrimSpace(string(b))
		if val == "" {
			val = def
		}
		break
	}
	return
}

// ReadBool 等待用戶 輸入 bool
func (r Reader) ReadBool(placeholder string) (val bool) {
	for {
		fmt.Printf("%s $ ", placeholder)
		b, _, e := r.rd.ReadLine()
		if e != nil {
			fmt.Println(e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "true" || str == "t" ||
			str == "yes" || str == "y" ||
			str == "1" {
			val = true
			break
		} else if str == "false" || str == "f" ||
			str == "no" || str == "n" ||
			str == "0" {
			val = false
			break
		}
	}
	return
}

// ReadBoolDefault 等待用戶 輸入 bool
func (r Reader) ReadBoolDefault(placeholder string, def bool) (val bool) {
	for {
		fmt.Printf("%s [%v] $ ", placeholder, def)
		b, _, e := r.rd.ReadLine()
		if e != nil {
			fmt.Println(e)
			continue
		}
		str := strings.ToLower(strings.TrimSpace(string(b)))
		if str == "true" || str == "t" ||
			str == "yes" || str == "y" ||
			str == "1" {
			val = true
			break
		} else if str == "false" || str == "f" ||
			str == "no" || str == "n" ||
			str == "0" {
			val = false
			break
		}
		val = def
		break
	}
	return
}
