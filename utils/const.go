package utils

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

const (
	// Separator 字符串 分隔符號
	Separator = "`"
)

// SplitInt64 獲取 int64 值
func SplitInt64(str string) []int64 {
	str = strings.TrimSpace(str)
	if str == "" {
		return nil
	}
	strs := strings.Split(str, Separator)
	arrs := make([]int64, 0, len(strs))
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		v, _ := strconv.ParseInt(str, 10, 64)
		if v != 0 {
			arrs = append(arrs, v)
		}
	}
	return arrs
}

// JoinUint32 .
func JoinUint32(vals []uint32) (str string, e error) {
	switch len(vals) {
	case 0:
	case 1:
		str = fmt.Sprint(vals[0])
	case 2:
		str = fmt.Sprintf("%v%v%v", vals[0], Separator, vals[1])
	default:
		str, e = joinUint32(vals)
	}
	return
}
func joinUint32(vals []uint32) (str string, e error) {
	var buf bytes.Buffer
	for i := 0; i < len(vals); i++ {
		val := vals[i]
		if i == 0 {
			_, e = buf.WriteString(fmt.Sprint(val))
		} else {
			_, e = buf.WriteString(fmt.Sprintf("%v%v", Separator, val))
		}
		if e != nil {
			return
		}
	}
	str = buf.String()
	return
}

// JoinInt32 .
func JoinInt32(vals []int32) (str string, e error) {
	switch len(vals) {
	case 0:
	case 1:
		str = fmt.Sprint(vals[0])
	case 2:
		str = fmt.Sprintf("%v%v%v", vals[0], Separator, vals[1])
	default:
		str, e = joinInt32(vals)
	}
	return
}
func joinInt32(vals []int32) (str string, e error) {
	var buf bytes.Buffer
	for i := 0; i < len(vals); i++ {
		val := vals[i]
		if i == 0 {
			_, e = buf.WriteString(fmt.Sprint(val))
		} else {
			_, e = buf.WriteString(fmt.Sprintf("%v%v", Separator, val))
		}
		if e != nil {
			return
		}
	}
	str = buf.String()
	return
}

// SplitInt32 獲取 int32 值
func SplitInt32(str string) []int32 {
	str = strings.TrimSpace(str)
	if str == "" {
		return nil
	}
	strs := strings.Split(str, Separator)
	arrs := make([]int32, 0, len(strs))
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		v, _ := strconv.ParseInt(str, 10, 32)
		if v != 0 {
			arrs = append(arrs, int32(v))
		}
	}
	return arrs
}

// SplitUint64 獲取 uint64 值
func SplitUint64(str string) []uint64 {
	str = strings.TrimSpace(str)
	if str == "" {
		return nil
	}
	strs := strings.Split(str, Separator)
	arrs := make([]uint64, 0, len(strs))
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		v, _ := strconv.ParseUint(str, 10, 64)
		if v != 0 {
			arrs = append(arrs, v)
		}
	}
	return arrs
}

// SplitUint32 獲取 int64 值
func SplitUint32(str string) []uint32 {
	str = strings.TrimSpace(str)
	if str == "" {
		return nil
	}
	strs := strings.Split(str, Separator)
	arrs := make([]uint32, 0, len(strs))
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		v, _ := strconv.ParseUint(str, 10, 32)
		if v != 0 {
			arrs = append(arrs, uint32(v))
		}
	}
	return arrs
}
