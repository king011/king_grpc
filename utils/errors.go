package utils

import "errors"

// ErrSessionNil session == nil
var ErrSessionNil = errors.New("session nil")

// ErrIDNil id == nil
var ErrIDNil = errors.New("id nil")

// ErrAlreadyLocked 資源已經被鎖定 無法鎖定
var ErrAlreadyLocked = errors.New("already locked")
