package utils

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

var _BasePath string

// BasePath 返回 可執行檔案 所在 檔案夾
func BasePath() string {
	if _BasePath != "" {
		return _BasePath
	}
	filename, e := exec.LookPath(os.Args[0])
	if e != nil {
		log.Fatalln(e)
	}

	filename, e = filepath.Abs(filename)
	if e != nil {
		log.Fatalln(e)
	}
	_BasePath = filepath.Dir(filename)
	return _BasePath
}
