package utils

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
)

// HashFile 計算 檔案 hash 值
func HashFile(filename string) (v string, e error) {
	f, e := os.Open(filename)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}
	v, e = HashReader(f)
	f.Close()
	return
}

// HashReader 計算 hash 值
func HashReader(r io.Reader) (v string, e error) {
	h := md5.New()
	_, e = io.Copy(h, r)
	if e != nil {
		return
	}
	v = hex.EncodeToString(h.Sum(nil))
	return
}

// HashReader2 計算 hash 值
func HashReader2(r io.Reader) (v string, n int64, e error) {
	h := md5.New()
	n, e = io.Copy(h, r)
	if e != nil {
		return
	}
	v = hex.EncodeToString(h.Sum(nil))
	return
}
